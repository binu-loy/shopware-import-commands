<?php

namespace App\Command\goldenpeanut;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class CustomerCreateCommand extends Command {

    protected $apiClient;
    protected $customers;
    protected $billings;
    protected $base;
    protected $csvFileName;
    protected $csvBillingFileName;
    protected $dumppath;
    protected static $defaultName = 'app:goldenpeanut:customer:create';

    protected function configure() {
        $this
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }


        $fields = $this->configureFields();
        $customersCollection = $this->customers;
        $billings = $this->billings;
        $countries = $this->getCountries();

        $customers = $this->prepareCustomers($output, $fields, $customersCollection, $billings, $countries);
        $insertedArray = $this->executePost($output, $customers);

//        $output->writeln('Creating dump');
//        $this->createDump();
    }

    private function configureFields() {

        return [
            'account_id_ac' => 0,
            'title_ac' => 1,
            'email_ac' => 2,
            'phone_ac' => 3,
            'fax_ac' => 4,
            'dob_ac' => 5,
            'username_ac' => 6,
            'password_ac' => 7,
            'record_created_ac' => 8,
            'address_name_bi' => 1,
            'account_id_bi' => 2,
            'salutation_bi' => 3,
            'first_name_bi' => 4,
            'last_name_bi' => 5,
            'company_bi' => 6,
            'address_1_bi' => 7,
            'address_2_bi' => 8,
            'city_bi' => 9,
            'zipcode_bi' => 10,
            'country_bi' => 11
        ];
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';

        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csvFileName = $publicPath . 'csvData/goldenpeanut/customers.csv';
        $this->csvBillingFileName = $publicPath . 'csvData/goldenpeanut/billing.csv';


        $this->dumppath = $publicPath . 'dump/goldenpeanut/';
        $this->customers = $this->base->readCSV($this->csvFileName);
        $this->billings = $this->base->readCSV($this->csvBillingFileName);
        $this->apiClient = $this->base->getClient($url, $username, $apiKey, false);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

//        $url = 'https://www.goldenpeanut.at/api';
//        $username = 'loyadmin';
//        $apiKey = 'kfcqukqyC1TJEI72my13VZh9TbLGsvTVMZgG4Hkt';
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function prepareCustomers(OutputInterface $output, $fields, $customersCollection, $billings, $countries) {

        $customerArray = [];
        foreach ($customersCollection as $key => $value) {
            $customerArray[$value[$fields['account_id_ac']]]['account_info'] = $value;
        }
        foreach ($billings as $key => $billing) {
            if (isset($customerArray[$billing[$fields['account_id_bi']]])) {
                $customerArray[$billing[$fields['account_id_bi']]][$billing[$fields['address_name_bi']]] = $billing;
            }
        }

        foreach ($customerArray as $customer) {
            $customerData = $this->loadCustomer($customer, $fields, $countries);
            dump($customerData);
            if ($customerData != NULL) {
                $customers[] = $customerData;
            } else {
                dump('No billing info for ' . $customer['account_info'][$fields['email_ac']]);
            }
        }
        return $customers;
    }

    private function executePost($output, $customers) {

        $errorArray = [];
        $insertCount = 0;
        $insertedArray = [];

        $output->write('---------------------------', true);
        foreach ($customers as $customer) {
            $status = $this->apiClient->post('customers', $customer);
            if (!$status['success']) {

                $this->base->writeLog($customer['email'], TRUE, 'B');
                $output->write('Failed ' . $customer['email'], true);
                $errorArray[] = $customer['email'];
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $customer['email'] . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
            $insertedArray[$customer['lastname']] = $status['data']['id'];
//            sleep(1);
        }
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }

        return $insertedArray;
    }

    private function loadCustomer($item, $fields, $countries) {

        $salutation = 'mr';
        if (isset($item['billing'])) {
            switch ($item['billing'][$fields['salutation_bi']]) {
                case 'H':
                    $salutation = 'ms';
                case 'F':
                    $salutation = 'mr';
                case 'C':
                    $salutation = 'co';
            }
            $title = $item['account_info'][$fields['title_ac']];

            $infos = [];
            foreach ($item as $key => $values) {

                if ($key !== 'account_info') {

                    $countryName = strtolower($values[$fields['country_bi']]);
                    $selectedCountry = '';
                    if ($countryName != '' && isset($countries[$countryName])) {
                        $selectedCountry = $countries[$countryName];
                    }

                    $infos[$key]['salutation'] = $salutation;
                    $infos[$key]['title'] = $title;
                    $infos[$key]['firstname'] = $values[$fields['first_name_bi']];
                    $infos[$key]['lastname'] = $values[$fields['last_name_bi']];
                    $infos[$key]['company'] = $values[$fields['company_bi']];
                    $infos[$key]['street'] = $values[$fields['address_1_bi']];
                    $infos[$key]['additionalAddressLine1'] = $values[$fields['address_2_bi']];
                    $infos[$key]['city'] = $values[$fields['city_bi']];
                    $infos[$key]['zipcode'] = $values[$fields['zipcode_bi']];
                    $infos[$key]['country'] = $selectedCountry;
                    $infos[$key]['phone'] = $values[$fields['country_bi']];
                    $infos[$key]['fax'] = $values[$fields['country_bi']];
                }
            }

            $firstLoginObj = new \DateTime($item['account_info'][$fields['record_created_ac']]);
            $firstLogin = $firstLoginObj->format('Y-m-d');

            $dob = $item['account_info'][$fields['dob_ac']];
            $birthDayObj = date("Y-m-d", strtotime($dob));


            $customer = [
                'salutation' => $salutation,
                'firstname' => trim($item['billing'][$fields['first_name_bi']]),
                'lastname' => trim($item['billing'][$fields['last_name_bi']]),
                'email' => trim($item['account_info'][$fields['email_ac']]),
                'birthday' => $birthDayObj,
                'encoder' => 'bcrypt',
                'password' => $item['account_info'][$fields['password_ac']],
                'firstLogin' => $firstLogin,
                'accountMode' => 0,
                'active' => true,
            ];
            if ($title != '') {
                $customer['title'] = $title;
            }
            if (isset($infos['billing'])) {
                $customer['billing'] = $infos['billing'];
            }
            if (isset($infos['shipping'])) {
                $customer['shipping'] = $infos['shipping'];
            }

            return $customer;
        } else {
            return null;
        }
    }

    private function getCountries() {

        $countriesDetails = $this->apiClient->get('countries');
        $countries = [];
        foreach ($countriesDetails['data'] as $country) {
            $countries[strtolower($country['name'])] = $country['id'];
        }
        return $countries;
    }

}
