<?php

namespace App\Command\Common;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ArticleRemoveCommand extends Command {

    protected $stage;
    protected $apiClient;
    protected $categoryContent;
    protected $base;
    protected $dumpCsv;
    protected $insertArray;
    protected static $defaultName = 'app:common:article-remove';

    protected function configure() {
        $this
                ->addOption('from', 'f', InputOption::VALUE_REQUIRED, '', false)
                ->addOption('to', 't', InputOption::VALUE_REQUIRED, '', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->base = new BaseController();

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';

        $this->apiClient = $this->base->getClient($url, $username, $apiKey, false);

        $from = $input->getOption('from');
        $to = $input->getOption('to');

        if (!$to) {
            $to = $from;
        }

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Url is : ' . $url . ' Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }

        foreach (range($from, $to) as $value) {
            $articleId = $value;
            $status = $this->apiClient->delete('articles/' . $articleId);
            if ($status['success'] === true) {
                $msg = 'Removed article id : ' . $articleId;
            } else {
                $msg = 'Failed article id : ' . $articleId;
            }
            $this->base->writeLog($msg, TRUE);
            dump($msg);
        }
        die;
    }

}
