<?php

namespace App\Command\Common;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class MediaRemoveCommand extends Command {

    protected $stage;
    protected $apiClient;
    protected $categoryContent;
    protected $base;
    protected $dumpCsv;
    protected $insertArray;
    protected static $defaultName = 'app:common:media-remove';

    protected function configure() {
        $this
                ->addOption('from', 'f', InputOption::VALUE_REQUIRED, '', false)
                ->addOption('to', 't', InputOption::VALUE_REQUIRED, '', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';

        $this->base = new BaseController();
        $this->apiClient = $this->base->getClient($url, $username, $apiKey, false);

        $from = $input->getOption('from');
        $to = $input->getOption('to');

        if (!$to) {
            $to = $from;
        }

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }

        foreach (range($from, $to) as $value) {

            $media = $this->apiClient->get('media/' . $value);
            $mediaId = $value;
            if ($media['data']['albumId'] === -1) {
                $status = $this->apiClient->delete('media/' . $value);
                if ($status['success'] === true) {
                    $msg = 'Removed media id : ' . $mediaId;
                } else {
                    $msg = 'Failed media id : ' . $mediaId;
                }
            } else {
                $msg = 'Not an article media : ' . $mediaId;
            }
            $this->base->writeLog($msg, TRUE);
            dump($msg);
        }
        die;
    }

}
