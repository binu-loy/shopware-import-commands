<?php

namespace App\Command\Common;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class CategoryRemoveCommand extends Command {

    protected $stage;
    protected $apiClient;
    protected $categoryContent;
    protected $base;
    protected $dumpCsv;
    protected $insertArray;
    protected static $defaultName = 'app:common:category-remove';

    protected function configure() {
        $this
                ->addOption('from', 'f', InputOption::VALUE_REQUIRED, '', false)
                ->addOption('to', 't', InputOption::VALUE_REQUIRED, '', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
//        $url = 'https://www.wap-nilfisk-alto-shop.de/stage/api';
//        $username = 'frankwerne';
//        $apiKey = 'aXK0Wzed8H6vx9zST1S6VsDJKoqaZeWTva10hMLq';
        $this->base = new BaseController();
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);

        $from = $input->getOption('from');
        $to = $input->getOption('to');

        if (!$to) {
            $to = $from;
        }

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }

        foreach (range($from, $to) as $value) {
            $status = $this->apiClient->delete('categories/' . $value);
            dump($status);
        }
        die;
    }

}
