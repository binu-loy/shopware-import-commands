<?php

namespace App\Command\schmuckzicke;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class CustomerCreateCommand extends Command {

    protected $apiClient;
    protected $customers;
    protected $base;
    protected $csvFileName;
    protected $dumppath;
    protected static $defaultName = 'app:schmuckzicke:customer:create';

    protected function configure() {
        $this
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }


        $fields = $this->configureFields();
        $customersCollection = $this->customers;
        $countries = $this->getCountries();

        $customers = $this->prepareArticle($output, $fields, $customersCollection, $countries);
        $insertedArray = $this->executePost($output, $customers);

        $output->writeln('Creating dump');
        $this->createDump();
    }

    private function configureFields() {

        return [
            'match_code' => 1,
            'salutation' => 2,
            'firstname' => 3,
            'lastname' => 4,
            'street' => 5,
            'zipcode' => 6,
            'city' => 7,
            'country' => 8,
            'email' => 9,
            'phone' => 10,
            'remarks' => 11,
            'free_field' => 12
        ];
    }

    private function initValues() {

        $url = 'http://localhost/schmuckzicke/api';
        $username = 'loyadmin';
        $apiKey = 'h4do45zRx9x0RgkSiSMZirZDs0UZ6IT33kBXcnf5';

        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csvFileName = $publicPath . 'csvData/schmuckzicke/customers.csv';

        $this->dumppath = $publicPath . 'dump/robeonline/';
        $this->customers = $this->base->readCSV($this->csvFileName);
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $url = 'https://www.schmuckzicke.at/api';
        $username = 'loyadmin';
        $apiKey = 'kfcqukqyC1TJEI72my13VZh9TbLGsvTVMZgG4Hkt';
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function prepareArticle(OutputInterface $output, $fields, $customersCollection, $countries) {

        $articles = [];

        foreach ($customersCollection as $key => $customer) {

            $customers[] = $this->loadCustomer($customer, $fields, $countries);
        }
        $output->writeln([
            'Article data prepared.',
            'count of articles ' . count($customers),
        ]);
        return $customers;
    }

    private function executePost($output, $customers) {

        $errorArray = [];
        $insertCount = 0;
        $insertedArray = [];

        $output->write('---------------------------', true);
        foreach ($customers as $customer) {
            $status = $this->apiClient->post('customers', $customer);
            if (!$status['success']) {

//                $output->write('Failed ' . $article['mainDetail']['number'], true);
                $errorArray[] = $customer['email'];
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
//                $output->write("Success " . $article['mainDetail']['number'] . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
            $insertedArray[$customer['lastname']] = $status['data']['id'];
//            sleep(1);
        }
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }

        return $insertedArray;
    }

    private function loadCustomer($item, $fields, $countries) {

        $salutation = $item[$fields['salutation']] == 'Frau' ? 'ms' : 'mr';
        $countryName = $item[$fields['country']];
        $selectedCountry = '';
        if ($countryName != '') {
            $selectedCountry = $countries[$countryName];
        }


        $customer = [
            'salutation' => $salutation,
            'firstname' => $item[$fields['firstname']],
            'lastname' => $item[$fields['lastname']],
            'email' => $item[$fields['email']],
            'billing' => [
                'salutation' => $salutation,
                'firstname' => $item[$fields['firstname']],
                'lastname' => $item[$fields['lastname']],
                'street' => $item[$fields['street']],
                'street' => $item[$fields['street']],
                'zipcode' => $item[$fields['zipcode']],
                'city' => $item[$fields['city']],
                'country' => $item[$fields['country']],
                'country' => $selectedCountry,
                'phone' => $item[$fields['phone']],
            ],
            'attribute' => [
                'attr2' => $item[$fields['remarks']],
                'attr1' => $item[$fields['free_field']]
            ]
        ];

        return $customer;
    }

    private function getCountries() {

        $countriesDetails = $this->apiClient->get('countries');
        $countries = [];
        foreach ($countriesDetails['data'] as $country) {
            $countries[$country['name']] = $country['id'];
        }
        return $countries;
    }

    private function createDump() {

//        $fields = $this->configureFields();
//        $imageField = $fields['image'];
//        $missingArray = [];
//        $file = fopen($this->dumppath . "missing-images.csv", "w");
//        foreach ($articles as $key => $item) {
//            $exist = file_exists($this->imageLocationLocal . $item[$imageField] . '.jpg');
//            if (!$exist && !in_array($item[$imageField], $missingArray)) {
//                $missingArray[] = $item[$imageField];
//                fputcsv($file, [$item[$imageField]], ";");
//            }
//        }
//        fclose($file);
//        /*         * ******************************************************************************* */
//
//        /** Create csv for articles imported * */
//        $file = fopen($this->dumppath . "inserted-articles.csv", "w");
//        foreach ($insertedArticles as $key => $articleId) {
//            fputcsv($file, [$key, $articleId], ";");
//        }
//        fclose($file);
//        /*         * ******************************************************************************* */
    }

}
