<?php

namespace App\Command\schmuckzicke;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use App\Controller\schmuckzicke\ArticleController;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Input\InputOption;
use DateTime;

class ArticlePost extends Command {

    protected $apiClient;
    protected $categoryContent;
    protected $articleContent;
    protected $base;
    protected $articleController;
    private $articleCsvFileName;
    private $insertedArticleCsvFileName;
    private $findInInserted;
    private $categoryCsvFileName;
    private $numberUrl;
    private $idNumber;
    private $stage;
    private $dumpPath;
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:schmuckzicke:post-articles';

    protected function configure() {
        $this
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->setDescription('Article post command for schmuckzicke')
                ->setHelp('schmuckzicke - post - article ');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues();
        $this->createRewriteDump();
        die;

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }

        $insertCount = 0;

        $articles = $this->prepareArticle($input, $output);
        $errorArray = [];
        $output->write('---------------------------', true);
        foreach ($articles as $article) {
            $status = $this->apiClient->post('articles', $article);
            $orderNumber = $article['mainDetail']['number'];
            if (!$status['success']) {
                $this->base->writeLog($orderNumber, TRUE, 'B');

                $output->write('Failed ' . $article['mainDetail']['number'], true);
                $errorArray[] = $article['mainDetail']['number'];
            } else {
                $insertCount++;
                $output->write("Success " . $article['mainDetail']['number'] . "( " . $insertCount . " )", true);
            }
            $this->idNumber[$orderNumber] = $status['data']['id'];
        }
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }

        $this->createDump();
//        $this->createRewriteDump();
        die;
    }

    private function prepareArticle(InputInterface $input, OutputInterface $output) {

        $articleBatch = [];
        $article = [];
        $articleContent = $this->updateCategories();

        foreach ($articleContent as $key => $item) {
            $article['name'] = $item[2];
            $article['descriptionLong'] = $item[3];
            $article['active'] = true;
            $article['supplier'] = $item[10];
            $taxrate = $item[11] == 19 ? 20 : $item[11];
            $article['tax'] = $taxrate;
            $article['added'] = $item[26];

            $categories = [];
            if ($item[19] != '') {
                $categoryToInsert = $item[19];
                if ($categoryToInsert == 27) {
                    $categoryToInsert = 69;
                }
                array_push($categories, ['id' => $categoryToInsert]);
            }
            if ($item[20] != '') {
                $categoryToInsert = $item[20];
                if ($categoryToInsert == 27) {
                    $categoryToInsert = 69;
                }
                array_push($categories, ['id' => $categoryToInsert]);
            }
            if ($item[21] != '') {
                $categoryToInsert = $item[21];
                if ($categoryToInsert == 27) {
                    $categoryToInsert = 69;
                }
                array_push($categories, ['id' => $categoryToInsert]);
            }
            if ($item[22] != '') {
                $categoryToInsert = $item[22];
                if ($categoryToInsert == 27) {
                    $categoryToInsert = 69;
                }
                array_push($categories, ['id' => $categoryToInsert]);
            }
            if ($item[23] != '') {
                $categoryToInsert = $item[23];
                if ($categoryToInsert == 27) {
                    $categoryToInsert = 69;
                }
                array_push($categories, ['id' => $categoryToInsert]);
            }

            $article['categories'] = $categories;


            $images = [];
            if ($item[4] != '') {
                array_push($images, ['link' => $item[4]]);
            }
            if ($item[5] != '') {
                array_push($images, ['link' => $item[5]]);
            }
            if ($item[6] != '') {
                array_push($images, ['link' => $item[6]]);
            }
            if ($item[7] != '') {
                array_push($images, ['link' => $item[7]]);
            }
            if ($item[8] != '') {
                array_push($images, ['link' => $item[8]]);
            }
            if ($item[9] != '') {
                array_push($images, ['link' => $item[9]]);
            }


            $inStock = $item[24] === 'enabled' ? 0 : 1;

            $mainDetail = [];
            $mainDetail['number'] = $item[1];
            $mainDetail['inStock'] = $inStock;
            $mainDetail['active'] = true;


            $prices = [];
            $priceEK = [
                'customerGroupKey' => 'EK',
                'price' => $item[12],
                'pseudoPrice' => $item[13],
                'basePrice' => $item[14]
            ];
            $priceH = [
                'customerGroupKey' => 'H',
                'price' => $item[15],
                'pseudoPrice' => $item[16],
                'basePrice' => $item[17]
            ];

            $prices = [$priceEK, $priceH];
            $mainDetail['prices'] = $prices;


            $article['images'] = $images;
            $article['mainDetail'] = $mainDetail;
            $articleBatch[] = $article;
            $url = str_replace("https://www.schmuckzicke.de/", "", $item[25]);
            $this->numberUrl[$item[1]] = $url;
        }

        $output->writeln([
            'Article data prepared.'
        ]);
        dump(count($articleContent) . ' total rows in csv');
        dump(count($this->findInInserted) . ' found as inserted.');
        dump(count($articleBatch) . ' have to insert');
        return $articleBatch;
    }

    private function initValues() {

        $this->base = new BaseController();
        $this->articleController = new ArticleController();
        $publicPath = __DIR__ . "/../../../public/";
        $this->articleCsvFileName = $publicPath . 'csvData/schmuckzicke/articles.csv';
        $this->categoryCsvFileName = $publicPath . 'csvData/schmuckzicke/categories.csv';
        $this->dumppath = $publicPath . 'dump/schmuckzicke/';
        $this->findInInserted = [];

        $url = 'http://localhost/schmuckzicke/api';
        $username = 'loyadmin';
        $apikey = 'h4do45zRx9x0RgkSiSMZirZDs0UZ6IT33kBXcnf5';
        $this->apiClient = $this->base->getClient($url, $username, $apikey);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $url = 'https://www.schmuckzicke.at/api';
        $username = 'loyadmin';
        $apiKey = 'kfcqukqyC1TJEI72my13VZh9TbLGsvTVMZgG4Hkt';
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function updateCategories() {

        $oldCategory = [];
        $newCategory = [];
        $categoryContent = $this->base->readCSV($this->categoryCsvFileName);
        foreach ($categoryContent as $category) {
            $oldCategory[] = $category[0];
            $newCategory[] = $category[2];
        }

        $this->articleContent = $this->base->readCSV($this->articleCsvFileName);
        foreach ($this->articleContent as $index => $item) {
            if (in_array($item[19], $oldCategory) && $item[19] != '') {
                $categoryKey = array_search($item[19], $oldCategory);
                $this->articleContent[$index][19] = $newCategory[$categoryKey];
            }

            if (in_array($item[20], $oldCategory) && $item[20] != '') {
                $categoryKey = array_search($item[20], $oldCategory);
                $this->articleContent[$index][20] = $newCategory[$categoryKey];
            }

            if (in_array($item[21], $oldCategory) && $item[21] != '') {
                $categoryKey = array_search($item[21], $oldCategory);
                $this->articleContent[$index][21] = $newCategory[$categoryKey];
            }

            if (in_array($item[22], $oldCategory) && $item[22] != '') {
                $categoryKey = array_search($item[22], $oldCategory);
                $this->articleContent[$index][22] = $newCategory[$categoryKey];
            }

            if (in_array($item[23], $oldCategory) && $item[23] != '') {
                $categoryKey = array_search($item[23], $oldCategory);
                $this->articleContent[$index][23] = $newCategory[$categoryKey];
            }
        }

        return $this->articleContent;
    }

    private function createDump() {

        $file = fopen($this->dumppath . "find_in_inserted.csv", "w");
        foreach ($this->findInInserted as $item) {
            fputcsv($file, $item, ";");
        }
        fclose($file);

        $file = fopen($this->dumppath . "id_number_dump.csv", "w");
        foreach ($this->idNumber as $key => $item) {
            fputcsv($file, [$key, $item], ";");
        }
        fclose($file);

        $file = fopen($this->dumppath . "number_url_dump.csv", "w");
        foreach ($this->numberUrl as $key => $item) {
            fputcsv($file, [$key, $item], ";");
        }
        fclose($file);
    }

    private function createRewriteDump() {

        $idNumberArray = [];
        $numberUrlArray = [];
        $rewriteArray = [];

        $idNumber = $this->base->readCSV("/home/binu.abraham@beo.in/Documents/dump/id_number_dump.csv");
        foreach ($idNumber as $value) {
            $idNumberArray[$value[1]] = $value[0];
        }

        $this->articleContent = $this->base->readCSV("/home/binu.abraham@beo.in/Documents/dump/articles.csv", "|");
        foreach ($this->articleContent as $value) {
            $numberUrlArray[$value[1]] = $value[25];
        }


        foreach ($idNumberArray as $key => $value) {
            $rewriteArray[$value] = $numberUrlArray[$key];
        }


        $file = fopen("/home/binu.abraham@beo.in/Documents/dump/new_url.csv", "w");
        foreach ($rewriteArray as $key => $value) {

            if ($value != '') {

                $url = str_replace('https://www.schmuckzicke.de/', '', $value);
                fputcsv($file, [$key, $url], ";");
            }
        }
        fclose($file);
    }

}
