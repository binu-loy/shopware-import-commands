<?php

namespace App\Command\erimaTeamsportandmore;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ImportArticle extends Command {

    protected $stage;
    protected $apiClient;
    protected $articleContent;
    protected $base;
    protected $csvFileName;
    protected $imageLocationLocal;
    protected $imageLocationRemote;
    protected $missingImageFile;
    protected $category;
    protected $outputCsvFileName;
    protected $dumpCsv;
    protected static $defaultName = 'app:teamsportandmore:import-articles';

    protected function configure() {
        $this
                ->setDescription('Article post command for teamsportandmore - supplier erima')
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->addOption('dumpCsv', 'd', InputOption::VALUE_OPTIONAL, 'If s available then run dump csv function', false)
                ->setHelp('teamsportandmore - post - article ');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
//            $this->initStage($input, $output);
        }

        $fields = $this->configureFields();
        $articleCollection = $this->articleContent;

        $imageCollection = $this->importImages($articleCollection, $fields);
        $articles = $this->prepareArticle($output, $fields, $articleCollection, $imageCollection);
        $insertedArray = $this->executePost($output, $articles);

        $this->dumpCsv = $input->getOption('dumpCsv');
        if ($this->dumpCsv !== false) {
            $this->createDump($articleCollection, $insertedArray, $imageCollection);
        }
    }

    private function configureFields() {

        return [
            'order_number' => 0,
            'ean' => 1,
            'article_name' => 9,
            'supplierId' => 50,
            'descriptions' => [15, 21],
            'color' => 29,
            'size' => 35,
            'price' => 42,
            'images' => [46, 47, 48, 49]
        ];
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'amD1Nm9fCqDl4T6rLEFhLM5cas29xHS6RDzuAZ1l';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->outputCsvFileName = $publicPath . 'csvOutput/erima_teamsportandmore_output.csv';
        $this->outputCsvImageFileName = $publicPath . 'csvOutput/erima_teamsportandmore_output_images.csv';
        $this->csvFileName = $publicPath . 'csvData/erima_teamsportandmore.csv';
        $this->imageLocationLocal = '/home/binu.abraham@beo.in/Documents/Works doing/teamsport_images/erima_teamsportandmore/';
        $this->imageLocationRemote = '/home/binu.abraham@beo.in/Documents/Works doing/teamsport_images/erima_teamsportandmore/';
        $this->missingImageFile = $publicPath . 'csvOutput/erima_teamsportandmore_missing_image.csv';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $this->category = 7;
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $this->imageLocationRemote = '/home/teakpjjw/import_images/erima_teamsportandmore/';
        $url = 'https://www.teamsportandmore.de/api';
        $username = 'admin';
        $apiKey = 'bTG2fzEgcv7KZ8r45SKTxWNN06b2fd7R3CHZl5zT';
        $this->category = 482;
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function prepareArticle(OutputInterface $output, $fields, $articleCollection, $imageCollection) {

        $set = [];
        $articlesArray = [];

        foreach ($articleCollection as $key => $item) {
            $articlesArray[$item[$fields['article_name']]][] = $item;
        }
        foreach ($articlesArray as $key => $variantItem) {

            $colourCodes = array_unique(array_column($variantItem, $fields['color']));
            $sizeCodes = array_unique(array_column($variantItem, $fields['size']));

            $article = [];
            $variants = [];
            foreach ($variantItem as $variantInder => $item) {
                if ($variantInder == 0) {
                    $article = $this->initialLoadArticle($article, $item, $fields);
                }
                $images = $this->loadImages($item, $fields, $imageCollection);

                $variant = $this->loadVariant($item, $fields, $images);
                $variants[] = $variant;
            }

            $variants[0]['isMain'] = true;
            $mainDetail = $variants[0];
            $article['mainDetail'] = $mainDetail;
            array_splice($variants, 0, 1);

            $configurationSet = $this->getConfigurationSet($colourCodes, $sizeCodes);
            $article['configuratorSet'] = $configurationSet;
            if (count($variants) > 0) {
                $article['variants'] = $variants;
            }

            $set[] = $article;
        }
        $output->writeln([
            'Article data prepared.',
            'count of articles ' . count($set),
            'total variants ' . count($articleCollection),
        ]);
        return $set;
    }

    private function executePost($output, $articles) {

        $errorArray = [];
        $insertCount = 0;
        $insertedArray = [];

        $output->write('---------------------------', true);
        foreach ($articles as $article) {
            $status = $this->apiClient->post('articles', $article);
            if (!$status['success']) {

                $output->write('Failed ' . $article['mainDetail']['number'], true);
                $errorArray[] = $article['mainDetail']['number'];
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $article['mainDetail']['number'] . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
            $insertedArray[$article['name']] = $status['data']['id'];
            sleep(1);
        }
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }

        return $insertedArray;
    }

    private function missingImages($articles) {

        $missingArray = [];
        $fields = $this->configureFields();
        foreach ($articles as $key => $item) {
            foreach ($fields['images'] as $field) {
                $exist = file_exists($this->imageLocationLocal . $item[$field]);
                if (!$exist && !in_array($item[$field], $missingArray)) {
                    $missingArray[] = $item[$field];
                }
            }
        }
        return $missingArray;
    }

    private function importImages($articles, $fields) {

        $imageCollection = [];
        foreach ($articles as $article) {
            foreach ($fields['images'] as $imageField) {
                if ($article[$imageField] != '' && !in_array($article[$imageField], array_keys($imageCollection))) {
                    $name = explode('.', $article[$imageField]);
                    $mediaData = [
                        'album' => -1,
                        'file' => $this->imageLocationRemote . $article[$imageField],
                        'name' => $name[0],
                        'description' => $name[0]
                    ];
                    $status = $this->apiClient->post('media', $mediaData);
//                    sleep(1);
                    $imageCollection[$article[$imageField]] = $status['data']['id'];
                }
            }
        }
        return $imageCollection;
    }

    private function initialLoadArticle($article, $item, $fields) {
        $article['name'] = $item[$fields['article_name']];
        if ($this->stage) {
            $article['supplierId'] = 1;
        } else {
            $article['supplier'] = 'Erima';
        }
        $article['active'] = true;
        $article['tax'] = 19;


        $description = "";
        foreach ($fields['descriptions'] as $descriptionField) {
            $description .= '<p>' . $item[$descriptionField] . '</p>';
        }
        $article['descriptionLong'] = $description;

        $article['categories'] = [
            ['id' => $this->category]
        ];

        return $article;
    }

    private function loadVariant($item, $fields, $images) {

        $variant = [];
        $colourCode = $this->getColourCode($item, $fields);
        $sizeCode = $item[$fields['size']];

        $variant['ean'] = $item[$fields['ean']];
        $variant['isMain'] = false;
        $variant['inStock'] = 1;
        $variant['number'] = $item[$fields['order_number']] . '-' . $sizeCode;
        $variant['active'] = TRUE;
        $variant['shippingTime'] = '3-5';


        $price = $item[$fields['price']];
        $priceEK = [
            'customerGroupKey' => 'EK',
            'price' => $price,
        ];
        $priceH = [
            'customerGroupKey' => 'H',
            'price' => $price,
        ];

        $prices = [$priceEK, $priceH];
        $variant['prices'] = $prices;

        $variant['configuratorOptions'] = [
            ['group' => 'Farbe', 'option' => $colourCode],
            ['group' => 'Größe', 'option' => $sizeCode]
        ];
        if (count($images) > 0) {
            $variant['images'] = $images;
        }

        return $variant;
    }

    private function loadImages($item, $fields, $imageCollection) {

        $images = [];
        $colourCode = $this->getColourCode($item, $fields);
        foreach ($fields['images'] as $imageField) {
            if ($item[$imageField] != '' && $imageCollection[$item[$imageField]] != '') {
                $images[] = [
                    'mediaId' => $imageCollection[$item[$imageField]],
                    'options' => [
                        [
                            [
                                'name' => $colourCode,
                            ]
                        ]
                    ]
                ];
            }
        }

        return $images;
    }

    private function getColourCode($item, $fields) {

        return ucwords($item[$fields['color']]);
    }

    private function getConfiguratorOptionColors($colors) {

        $options = array_map(function($color) {
            $capitalized = ucwords($color);
            return ['name' => $capitalized];
        }, $colors);

        return $options;
    }

    private function getConfiguratorOptionSizes($sizes) {

        $options = array_map(function($size) {
            return ['name' => $size];
        }, $sizes);

        return $options;
    }

    private function getConfigurationSet($colors, $sizes) {

        $configurationSetOptionsColour = $this->getConfiguratorOptionColors($colors);
        $configurationSetOptionsSize = $this->getConfiguratorOptionSizes($sizes);
        $configurationSet = [
            'type' => 2,
            'groups' => [
                [
                    'name' => 'Farbe',
                    'options' => $configurationSetOptionsColour
                ],
                [
                    'name' => 'Größe',
                    'options' => $configurationSetOptionsSize
                ]
            ]
        ];

        return $configurationSet;
    }

    private function createDump($articles, $insertedArticles = [], $imageCollection = []) {

        /** Create csv of missing images in the system * */
        $missingArray = $this->missingImages($articles);
        /*         * ******************************************************************************** */

        /** Create csv articles for which images are missing  * */
        $fields = $this->configureFields();
        $imageJoins = [];
        foreach ($fields['images'] as $imageField) {
            $images = array_column($articles, $imageField);
            $articleNames = array_column($articles, $fields['article_name']);
            $imageJoin = array_combine($images, $articleNames);
            unset($imageJoin[""]);
            $imageJoins = array_merge($imageJoins, $imageJoin);
        }

        $file = fopen($this->missingImageFile, "w");
        foreach ($missingArray as $missingImage) {
            fputcsv($file, [$missingImage, $imageJoins[$missingImage]], ';');
        }
        fclose($file);
        /*         * ******************************************************************************** */

        /** Create csv for articles imported * */
        $file = fopen($this->outputCsvFileName, "w");
        foreach ($insertedArticles as $key => $articleId) {
            fputcsv($file, [$key, $articleId], ";");
        }
        fclose($file);
        /*         * ******************************************************************************* */

        /** Create csv for images imported * */
        $file = fopen($this->outputCsvImageFileName, "w");
        foreach ($imageCollection as $key => $value) {
            fputcsv($file, [$key, $value], ";");
        }
        fclose($file);
        /*         * ****************************************************************************** */
    }

}
