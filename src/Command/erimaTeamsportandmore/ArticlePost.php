<?php

namespace App\Command\erimaTeamsportandmore;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;

class ArticlePost extends Command {

    protected $apiClient;
    protected $articleContent;
    protected $base;
    protected $csv;
    protected $csvFileName;
    protected $imageLocationLocal;
    protected $imageLocationRemote;
    protected $imageFields;
    protected $missingImageFile;
    protected $runStage;
    protected $category;
    protected $outputCsvFileName;
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:erima:teamsportandmore:post-articles';

    protected function configure() {
        $this
                ->setDescription('Article post command for teamsportandmore - supplier erima')
                ->setHelp('teamsportandmore - post - article ');
    }

    /**
     * Initialize values for the command
     */
    private function initValues() {

        $this->runStage = false;
        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'amD1Nm9fCqDl4T6rLEFhLM5cas29xHS6RDzuAZ1l';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csv = [
            'order_number' => 0,
            'ean' => 1,
            'article_name' => 9,
            'supplierId' => 50,
            'description' => 15,
            'description2' => 21,
            'color' => 29,
            'size' => 35,
            'price' => 42,
            'image1' => 46,
            'image2' => 47,
            'image3' => 48,
            'image4' => 49
        ];
        $this->outputCsvFileName = $publicPath . 'csvData/erima_teamsportandmore_output.csv';
        $this->csvFileName = $publicPath . 'csvData/erima_teamsportandmore.csv';
        /* To find missing images */
        $this->imageLocationLocal = '/home/binu.abraham@beo.in/Documents/Works doing/teamsport_images/erima_teamsportandmore/';
        /* To get file for import */
        $this->imageLocationRemote = '/home/binu.abraham@beo.in/Documents/Works doing/teamsport_images/erima_teamsportandmore/';
        $this->imageFields = ['image1', 'image2', 'image3', 'image4'];
        $this->missingImageFile = 'public/erima_teamsportandmore_missing.csv';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $this->category = 7;


        if ($this->runStage) {
            $this->imageLocationRemote = '/home/teakpjjw/import_images/erima_teamsportandmore/';
            $url = 'https://www.teamsportandmore.de/api';
            $username = 'admin';
            $apiKey = 'bTG2fzEgcv7KZ8r45SKTxWNN06b2fd7R3CHZl5zT';
            $this->category = 482;
        }
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues();
        $insertCount = 0;

        $articles = $this->prepareArticle($input, $output, $this->articleContent);

        $errorArray = [];
        $output->write('---------------------------', true);
        $file = fopen($this->outputCsvFileName, "w");
        foreach ($articles as $article) {
            $status = $this->apiClient->post('articles', $article);
            if (!$status['success']) {

                $output->write('Failed ' . $article['mainDetail']['number'], true);
                $errorArray[] = $article['mainDetail']['number'];
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $article['mainDetail']['number'] . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
            $articleId = $status['data']['id'];
            $articleName = $article['name'];
            fputcsv($file, [$articleName, $articleId], ";");
            sleep(1);
        }
        fclose($file);
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }
        die;
    }

    private function prepareArticle(InputInterface $input, OutputInterface $output, $articleCollection) {

        $set = [];
        $articlesArray = [];

        $imagesCollection = $this->importImages($this->articleContent);

        if (!$this->runStage) {
            $this->missingImages($articleCollection);
        }
        foreach ($articleCollection as $key => $item) {
            $articlesArray[$item[$this->csv['article_name']]][] = $item;
        }
        foreach ($articlesArray as $key => $variantItem) {
            $article = [];
            $colors = [];
            $colorNames = [];
            $sizes = [];
            $sizeNames = [];
            $variants = [];
            $configurationSetOptionsColour = [];
            $configurationSetOptionsSize = [];
            foreach ($variantItem as $variantInder => $item) {
                /** Only need to find once in an article* */
                if ($variantInder == 0) {
                    $article['name'] = $item[$this->csv['article_name']];
                    /** Supplier value changes as per stage or local * */
                    if ($this->runStage) {
                        $article['supplierId'] = 1;
                    } else {
                        $article['supplier'] = 'Erima';
                    }
                    $article['active'] = true;
                    $article['tax'] = 19;

                    $description = '<p>' . $item[$this->csv['description']] . '</p>';
                    $description .= '<p>' . $item[$this->csv['description2']] . '</p>';


                    $article['descriptionLong'] = $description;

                    $article['categories'] = [
                        ['id' => $this->category]
                    ];
                }

                $colourCode = ucwords($item[$this->csv['color']]);
                if (!in_array($colourCode, $colorNames)) {
                    array_push($colors, ['name' => $colourCode]);
                    array_push($colorNames, $colourCode);
                    $configurationSetOptionsColour[] = ['name' => $colourCode];
                }

                $sizeCode = $item[$this->csv['size']];
                if (!in_array($sizeCode, $sizeNames)) {
                    array_push($sizes, ['name' => $sizeCode]);
                    array_push($sizeNames, $sizeCode);
                    $configurationSetOptionsSize[] = ['name' => $sizeCode];
                }

                $images = [];

                foreach ($this->imageFields as $imageField) {
                    if ($item[$this->csv[$imageField]] != '' && $imagesCollection[$item[$this->csv[$imageField]]] != '') {
                        $images[] = [
                            'mediaId' => $imagesCollection[$item[$this->csv[$imageField]]],
                            'options' => [
                                [
                                    [
                                        'name' => $colourCode,
                                    ]
                                ]
                            ]
                        ];
                    }
                }

                $variant = [];
                $variant['ean'] = $item[$this->csv['ean']];
                $variant['isMain'] = false;
                $variant['inStock'] = 1;
                $variant['number'] = $item[$this->csv['order_number']] . '-' . $sizeCode;
                $variant['active'] = TRUE;
                $variant['shippingTime'] = '3-5';


                $price = $item[$this->csv['price']];
                $priceEK = [
                    'customerGroupKey' => 'EK',
                    'price' => $price,
                ];
                $priceH = [
                    'customerGroupKey' => 'H',
                    'price' => $price,
                ];

                $prices = [$priceEK, $priceH];
                $variant['prices'] = $prices;
                $configurationSet = [
                    'type' => 2,
                    'groups' => [
                        [
                            'name' => 'Farbe',
                            'options' => $configurationSetOptionsColour
                        ],
                        [
                            'name' => 'Größe',
                            'options' => $configurationSetOptionsSize
                        ]
                    ]
                ];
                $variant['configuratorOptions'] = [
                    ['group' => 'Farbe', 'option' => $colourCode],
                    ['group' => 'Größe', 'option' => $sizeCode]
                ];
                if (count($images) > 0) {
                    $variant['images'] = $images;
                }
                $variants[] = $variant;
            }

            $variants[0]['isMain'] = true;

            $mainDetail = $variants[0];
            $article['mainDetail'] = $mainDetail;
            array_splice($variants, 0, 1);
            $article['configuratorSet'] = $configurationSet;
            if (count($variants) > 0) {
                $article['variants'] = $variants;
            }


            $set[] = $article;
        }
        $output->writeln([
            'Article data prepared.',
            'count of articles ' . count($set),
            'total variants ' . count($articleCollection),
        ]);
        return $set;
    }

    /**
     * Get images names from csv which are not available in the image directory
     * As per max, we insert those articles without images
     */
    private function missingImages($articles) {

        $missingArray = [];
        foreach ($articles as $key => $item) {
            foreach ($this->imageFields as $field) {
                $exist = file_exists($this->imageLocationLocal . $item[$this->csv[$field]]);
                if (!$exist && !in_array($item[$this->csv[$field]], $missingArray)) {
                    $missingArray[] = $item[$this->csv[$field]];
                }
            }
        }
        if (count($missingArray) > 0) {
            $file = fopen($this->missingImageFile, "w");
            foreach ($missingArray as $image) {
                fputcsv($file, [$image]);
            }
            fclose($file);
        }
        return $missingArray;
    }

    private function importImages($articles) {

        $imageCollection = [];
        foreach ($articles as $article) {
            foreach ($this->imageFields as $imageField) {
                if ($article[$this->csv[$imageField]] != '' && !in_array($article[$this->csv[$imageField]], array_keys($imageCollection))) {
                    $name = explode('.', $article[$this->csv[$imageField]]);
                    $mediaData = [
                        'album' => -1,
                        'file' => $this->imageLocationRemote . $article[$this->csv[$imageField]],
                        'name' => $name[0],
                        'description' => $name[0]
                    ];
                    $status = $this->apiClient->post('media', $mediaData);
                    sleep(1);
                    $imageCollection[$article[$this->csv[$imageField]]] = $status['data']['id'];
                }
            }
        }
        return $imageCollection;
    }

}
