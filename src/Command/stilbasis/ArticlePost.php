<?php

namespace App\Command\stilbasis;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ArticlePost extends Command {

    protected $apiClient;
    protected $articleContent;
    protected $base;
    protected $articleController;
    protected $csv;
    protected $csvFileName;
    protected $runStage;
    protected $category;
    protected $supplier;
    protected $supplierValue;
    private $container;
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:stilbasis:post-articles';

    public function __construct(ContainerInterface $container) {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure() {
        $this
                ->setDescription('Article post command for stilbasis')
                ->setHelp('stilbasis - post - article ');
    }

    /**
     * Initialize values for the command
     */
    private function initValues($input, $output) {

        $this->runStage = false;
        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csv = [
            'variant_code' => 0,
            'article_name' => 1,
            'article_number' => 4,
            'color' => 5,
            'weight' => 6,
            'viison_customs_tariff_number' => 9,
            'ean' => 10,
            'price' => 11
        ];
        $this->csvFileName = $publicPath . 'csvData/stilbasis.csv';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $this->category = 2382;
        $this->supplier = $this->runStage ? 'supplierId' : 'supplier';
        $this->supplierValue = 'Not Given';
        $helper = $this->getHelper('question');


        if ($this->runStage) {

            $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue (Y/n)?', false, '/^Y/');
            if (!$helper->ask($input, $output, $question)) {
                $output->writeln([
                    'Operation terminated'
                ]);
                die;
            }

            $this->imageLocationRemote = '/home/teakpjjw/import_images/erima_teamsportandmore/';
            $url = 'https://www.teamsportandmore.de/stage/api';
            $username = 'admin';
            $apiKey = 'bTG2fzEgcv7KZ8r45SKTxWNN06b2fd7R3CHZl5zT';
            $this->category = 456;
            $this->supplierValue = 1;
        }
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $question = new ConfirmationQuestion('Please create a free text field named customstariff for this to work. Press enter to continue.', false);
        $this->initValues($input, $output);
        $insertCount = 0;

        $articles = $this->prepareArticle($input, $output, $this->articleContent);
        $errorArray = [];
        $output->write('---------------------------', true);
        foreach ($articles as $article) {
            $status = $this->apiClient->post('articles', $article);
            if (!$status['success']) {

                $output->write('Failed ' . $article['mainDetail']['number'], true);
                $errorArray[] = $article['mainDetail']['number'];
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $article['mainDetail']['number'] . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
        }
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }

    }

    private function prepareArticle(InputInterface $input, OutputInterface $output, $articleCollection) {

        $set = [];
        $articlesArray = [];

        foreach ($articleCollection as $key => $item) {
            $articlesArray[$item[$this->csv['article_number']]][] = $item;
        }
        foreach ($articlesArray as $key => $variantItem) {
            $article = [];
            $colors = [];
            $colorNames = [];
            $variants = [];
            $configurationSetOptionsColour = [];
            foreach ($variantItem as $variantInder => $item) {
                /** Only need to find once in an article* */
                if ($variantInder == 0) {
                    $article['name'] = $item[$this->csv['article_name']];
                    /** Supplier value changes as per stage or local * */
                    $article[$this->supplier] = $this->supplierValue;
                    $article['active'] = true;
                    $article['tax'] = 19;
                    $article['weight'] = $item[$this->csv['weight']];
                    $article['categories'] = [
                        ['id' => $this->category]
                    ];
                }

                $colourCode = ucwords($item[$this->csv['color']]);
                if (!in_array($colourCode, $colorNames)) {
                    array_push($colors, ['name' => $colourCode]);
                    array_push($colorNames, $colourCode);
                    $configurationSetOptionsColour[] = ['name' => $colourCode];
                }

                $variant = [];
                $variant['ean'] = $item[$this->csv['ean']];
                $variant['isMain'] = false;
                $variant['inStock'] = 1;
                $variant['number'] = $this->changeFormat($item[$this->csv['variant_code']]);
                $variant['active'] = TRUE;

                $price = $item[$this->csv['price']] * 1.19;
                $priceEK = [
                    'customerGroupKey' => 'EK',
                    'price' => $price,
                ];
                $priceH = [
                    'customerGroupKey' => 'H',
                    'price' => $price,
                ];

                $prices = [$priceEK, $priceH];
                $variant['prices'] = $prices;
                $configurationSet = [
                    'type' => 2,
                    'groups' => [
                        [
                            'name' => 'Farbe',
                            'options' => $configurationSetOptionsColour
                        ],
                    ]
                ];
                $variant['configuratorOptions'] = [
                    ['group' => 'Farbe', 'option' => $colourCode],
                ];
                $variant['attribute'] = [
                    'viisonCustomsTariffNumber' => $item[$this->csv['viison_customs_tariff_number']],
                ];
                $variants[] = $variant;
            }

            $variants[0]['isMain'] = true;

            $mainDetail = $variants[0];
            $article['mainDetail'] = $mainDetail;
            array_splice($variants, 0, 1);
            $article['configuratorSet'] = $configurationSet;
            if (count($variants) > 0) {
                $article['variants'] = $variants;
            }

            $set[] = $article;
        }
        $output->writeln([
            'Article data prepared.',
            'count of articles ' . count($set),
            'total variants ' . count($articleCollection),
        ]);
        return $set;
    }

    /**
     * Change variant code format 
     */
    private function changeFormat($variantCode) {
        $variantCode = str_replace('/', '_', $variantCode);
        $variantCode = str_replace('=', '-', $variantCode);
        return $variantCode;
    }

}
