<?php

namespace App\Command\robeonline;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ArticleCreateCommand extends Command {

    protected $apiClient;
    protected $articleContent;
    protected $base;
    protected $csvFileName;
    protected $imageLocationLocal;
    protected $imageLocationRemote;
    protected $missingImageFile;
    protected $baseCategory;
    protected $dumppath;
    protected $artCatMap;
    private $runFromFile;
    protected static $defaultName = 'app:robeonline:article:create';

    protected function configure() {
        $this
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->addOption('runFromFile', 'f', InputOption::VALUE_OPTIONAL, 'Use file to run', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }

        $this->runFromFile = $input->getOption('runFromFile');

        $fields = $this->configureFields();
        $articleCollection = $this->articleContent;
        dump('starting create missing file,');
        $this->createMissingImageDump($articleCollection);
        dump('completed create missing file,');

        /** Importing categories * */
        $output->writeln('Started inserting categories.');
        if ($this->runFromFile !== false) {
            $insertedCategories = $this->loadCategoryFromCsv();
            $this->artCatMap = $insertedCategories;
        } else {
            $insertedCategories = $this->createCategories($articleCollection);
            $this->categoryBasedDump($insertedCategories);
        }
        $output->writeln('Entered categories');

        /** Importing images * */
        $output->writeln('Importing images');
        if ($this->runFromFile !== false) {
            $imageCollection = $this->loadImageFromCsv();
        } else {
            $imageCollection = $this->importImages($articleCollection, $fields);
            $this->imageBaseDump($imageCollection);
        }
        $output->writeln('Images imported.');

        $articles = $this->prepareArticle($output, $fields, $articleCollection, $imageCollection);
        $insertedArray = $this->executePost($output, $articles);

        $output->writeln('Creating dump');
        $this->createDump($articleCollection, $insertedArray, $imageCollection, $insertedCategories);
    }

    private function configureFields() {

        return [
            'order_number' => 0,
            'article_name' => 1,
            'price' => 5, // *1.19
            'supplier' => 7,
            'description_long' => 11,
            'image' => 1,
            'category' => 4,
            'roAdapt2' => 8,
        ];
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csvFileName = $publicPath . 'csvOutput/robeonline/articles.csv';

        $this->imageLocationLocal = '/home/binu.abraham@beo.in/Documents/Works doing/robe-online.de/3 - XML Based Import/Sage-export/robeonline-images/';
        $this->imageLocationRemote = '/home/binu.abraham@beo.in/Documents/Works doing/robe-online.de/3 - XML Based Import/Sage-export/robeonline-images/';
        $this->dumppath = $publicPath . 'dump/robeonline/';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $this->baseCategory = 1472;
        $this->artCatMap = [];
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $this->imageLocationRemote = '/home/wapptbrt/www.wap-nilfisk-alto-shop.de/stage/robeonline-import-images/';
        $url = 'https://www.wap-nilfisk-alto-shop.de/api';
        $username = 'frankwerne';
        $apiKey = 'aXK0Wzed8H6vx9zST1S6VsDJKoqaZeWTva10hMLq';
        $this->baseCategory = 2263;
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function prepareArticle(OutputInterface $output, $fields, $articleCollection, $imageCollection) {

        $articles = [];

        foreach ($articleCollection as $key => $articleItem) {

            $articles[] = $this->loadArticle($articleItem, $fields, $imageCollection);
        }
        $output->writeln([
            'Article data prepared.',
            'count of articles ' . count($articles),
        ]);
        return $articles;
    }

    private function executePost($output, $articles) {

        $errorArray = [];
        $insertCount = 0;
        $insertedArray = [];

        $output->write('---------------------------', true);
        foreach ($articles as $article) {
            $status = $this->apiClient->post('articles', $article);
            if (!$status['success']) {

                $output->write('Failed ' . $article['mainDetail']['number'], true);
                $errorArray[] = $article['mainDetail']['number'];
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $article['mainDetail']['number'] . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
            $insertedArray[$article['name']] = $status['data']['id'];
            sleep(1);
        }
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }

        return $insertedArray;
    }

    private function importImages($articles, $fields) {

        $imageCollection = [];
        $imageField = $fields['image'];
        $images = array_filter(array_unique(array_column($articles, $imageField)));
        foreach ($images as $image) {

            $mediaData = [
                'album' => -1,
                'file' => $this->imageLocationRemote . $image . '.jpg',
                'name' => $image,
                'description' => $image
            ];
            $status = $this->apiClient->post('media', $mediaData);
            $imageCollection[$image] = $status['data']['id'];
        }
        return $imageCollection;
    }

    private function loadArticle($item, $fields, $imageCollection) {

        $article = [];
        $article['name'] = ucfirst($item[$fields['article_name']]);

        $supplier = ucfirst($item[$fields['supplier']]);
        if ($supplier == '') {
            $supplier = 'Robe';
        }

        $article['supplier'] = $supplier;
        $article['active'] = true;
        $article['tax'] = 19;
        $article['descriptionLong'] = ucfirst($item[$fields['description_long']]);
        $article['keywords'] = $item[$fields['order_number']];
        $article['metaTitle'] = ucfirst($item[$fields['article_name']]);
        $images = $this->loadImage($imageCollection, $item);
        if (count($images) > 0) {
            $article['images'] = [
                [
                    'mediaId' => $imageCollection[$item[$fields['image']]],
                ]
            ];
        }
        $article['attribute'] = [
            'roAdapt2' => $item[$fields['roAdapt2']]
        ];

        $price = $this->loadPrice($item[$fields['price']]);
        $shippingFree = $price > 250 ? TRUE : FALSE;
        $orderNumber = $this->loadOrderNumber($item[$fields['order_number']]);
        $article['mainDetail'] = [
            'number' => $orderNumber,
            'active' => true,
            'inStock' => 1000,
            'shippingFree' => $shippingFree,
            'prices' => [
                [
                    'customerGroupKey' => 'EK',
                    'pseudoPrice' => $price,
                    'price' => $price,
                ],
            ]
        ];

        $article['categories'] = [
            ['id' => $this->artCatMap[$orderNumber]]
        ];

        return $article;
    }

    private function loadImage($imageCollection, $item) {

        $fields = $this->configureFields();
        $images = [];
        if (isset($imageCollection[$item[$fields['image']]])) {
            $images = [
                [
                    'mediaId' => $imageCollection[$item[$fields['image']]],
                ]
            ];
        }
        return $images;
    }

    private function loadImageFromCsv() {

        $publicPath = __DIR__ . "/../../../public/";
        $entriesFromCsv = $this->base->readCSV($publicPath . '/dump/robeonline/inserted-images.csv');
        $imageCollection = array_combine(array_column($entriesFromCsv, 0), array_column($entriesFromCsv, 1));
        return $imageCollection;
    }

    private function loadCategoryFromCsv() {

        $publicPath = __DIR__ . "/../../../public/";
        $entriesFromCsv = $this->base->readCSV($publicPath . '/dump/robeonline/inserted-category-map.csv');
        $categoryCollection = array_combine(array_column($entriesFromCsv, 0), array_column($entriesFromCsv, 1));
        return $categoryCollection;
    }

    private function loadPrice($givenPrice) {

        $numericPrice = floatval($givenPrice);
        $priceAmount = $numericPrice * 1.19;
        $roundValue = round($priceAmount, 2);
        return $roundValue;
    }

    private function loadOrderNumber($orderNumber) {

        /** Change diacritic * */
        $orderNumber = htmlspecialchars($orderNumber);
        /** Replace a list of entries with hyphen * */
        $orderNumber = str_replace('/', '-', $orderNumber);
        $orderNumber = str_replace(',', '.', $orderNumber);
        $orderNumber = str_replace(' ', '_', $orderNumber);
        $orderNumber = $this->base->sonderzeichen($orderNumber);
        $orderNumber = 'R' . $orderNumber;

        return $orderNumber;
    }

    private function createCategories($articleCollection) {

        $categoryForEmpty = $this->createLeerCategory();
        $insertedCategories = [];
        foreach ($articleCollection as $item) {
            $fields = $this->configureFields();
            $orderNumber = $this->loadOrderNumber($item[$fields['order_number']]);


            $categoryFromCSV = $item[$fields['category']];
            $categoryArray = $this->createCatagoryArray($categoryFromCSV);

            if ($categoryFromCSV == '') {
                $this->artCatMap[$orderNumber] = $categoryForEmpty;
            } else {
                $categoryString = '';
                $parentCategory = $this->baseCategory;

                foreach ($categoryArray as $index => $category) {
                    if ($index == 0) {
                        $categoryString .= $category;
                    } else {
                        $categoryString = $categoryString . '.' . $category;
                    }
                    if (isset($insertedCategories[$categoryString])) {
                        $parentCategory = $insertedCategories[$categoryString];
                    } else {
                        $categoryData = [
                            'parentId' => $parentCategory,
                            'name' => $category
                        ];
                        $categoryStatus = $this->apiClient->post('categories', $categoryData);
                        $parentCategory = $categoryStatus['data']['id'];
                        $insertedCategories[$categoryString] = $categoryStatus['data']['id'];
                    }
                }
                $this->artCatMap[$orderNumber] = $parentCategory;
            }
        }

        return $insertedCategories;
    }

    /**
     * Create leer category for entries with empty category in csv
     */
    private function createLeerCategory() {

        $leerCategoryData = [
            'parentId' => $this->baseCategory,
            'name' => 'leer'
        ];
        $leerCategoryStatus = $this->apiClient->post('categories', $leerCategoryData);
        return $leerCategoryStatus['data']['id'];
    }

    private function createCatagoryArray($categoryString) {

        $categoryArray = [];
        $categoryString = str_replace('-', 'X', $categoryString);
        $categorySplitSpace = explode(' ', $categoryString);
        $numberArea = $categorySplitSpace[0];
        if ('.' != substr($numberArea, -1)) {
            $categoryArray = explode('.', $numberArea);
        }
        if (count($categorySplitSpace) > 1) {
            $stringArea = $categorySplitSpace[1];
            $stringAreaSlashBySpace = str_replace('/', ' ', $stringArea);
            $categoryArray[] = $stringAreaSlashBySpace;
        }
        return $categoryArray;
    }

    private function createDump($articles, $insertedArticles = [], $imageCollection = [], $insertedCategories) {

        $fields = $this->configureFields();
        $imageField = $fields['image'];
        $missingArray = [];
        $file = fopen($this->dumppath . "missing-images.csv", "w");
        foreach ($articles as $key => $item) {
            $exist = file_exists($this->imageLocationLocal . $item[$imageField] . '.jpg');
            if (!$exist && !in_array($item[$imageField], $missingArray)) {
                $missingArray[] = $item[$imageField];
                fputcsv($file, [$item[$imageField]], ";");
            }
        }
        fclose($file);
        /*         * ******************************************************************************* */

        /** Create csv for articles imported * */
        $file = fopen($this->dumppath . "inserted-articles.csv", "w");
        foreach ($insertedArticles as $key => $articleId) {
            fputcsv($file, [$key, $articleId], ";");
        }
        fclose($file);
        /*         * ******************************************************************************* */
    }

    private function imageBaseDump($imageCollection) {

        /** Create csv for images imported * */
        $file = fopen($this->dumppath . "inserted-images.csv", "w");
        foreach ($imageCollection as $key => $value) {
            fputcsv($file, [$key, $value], ";");
        }
        fclose($file);
        /*         * ****************************************************************************** */
    }

    private function categoryBasedDump($insertedCategories) {

        /** Create csv for article and category mapping as per category imported 
         * First column article order number and 
         * Second column category id * */
        $file = fopen($this->dumppath . "inserted-category-map.csv", "w");
        foreach ($this->artCatMap as $key => $value) {
            fputcsv($file, [$key, $value], ";");
        }
        fclose($file);
        /*         * ****************************************************************************** */

        /** Create csv for categories imported 
         * First column category tree 
         * Second column category id inserted 
         * */
        $file = fopen($this->dumppath . "inserted-categories.csv", "w");
        foreach ($insertedCategories as $key => $value) {
            fputcsv($file, [$key, $value], ";");
        }
        fclose($file);
        /*         * ****************************************************************************** */
    }

    private function createMissingImageDump($articles) {

        $fields = $this->configureFields();
        $imageField = $fields['image'];
        $missingArray = [];
        $file = fopen($this->dumppath . "missing-images.csv", "w");
        foreach ($articles as $key => $item) {
            $exist = file_exists($this->imageLocationLocal . $item[$imageField] . '.jpg');
            if (!$exist && !in_array($item[$imageField], $missingArray)) {
                $missingArray[] = $item[$imageField];
                fputcsv($file, [$item[$imageField] . '.jpg'], ";");
            }
        }
        fclose($file);
        /*         * ******************************************************************************* */
    }

}
