<?php

namespace App\Command\robeonline;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class XMLtoCSVCommand extends Command {

    private $xmlPath;
    private $outputCsvPath;
    protected static $defaultName = 'app:robeonline:article:xmltocsv';

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues();
        $xml = simplexml_load_file($this->xmlPath, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);

        $file = fopen($this->outputCsvPath, "w");
        foreach ($array['ROW'] as $value) {
            $priceinxml = $value['@attributes']['VKPREIS1'];
            $removedDot = str_replace('.', '', $priceinxml);
            $replaceCommaToDot = str_replace(',', '.', $removedDot);
            $article = [
                (string) $value['@attributes']['ARTNR'],
                $value['@attributes']['SUCHBEGRIFF'],
                $value['@attributes']['SUCHBEGRIFF2'],
                $value['@attributes']['BESTNR'],
                $value['@attributes']['WG'],
                $replaceCommaToDot,
                $value['@attributes']['BILD'],
                $value['@attributes']['FLD04'],
                $value['@attributes']['FLDRE04'],
                $value['@attributes']['ARTBESCHREIBUNG'],
                $value['@attributes']['BILDPATH'],
                $value['@attributes']['EPGLANGTXT'],
                $value['@attributes']['EPGKURZTXT'],
                $value['@attributes']['ZUSTEXT1'],
            ];



            fputcsv($file, $article, ";");
        }
        fclose($file);

        $output->writeln('<info>CSV created in the output folder.</info>');
    }

    private function initValues() {

        $publicPath = __DIR__ . "/../../../public/";
        $this->xmlPath = $publicPath . 'csvData/robeonline/articles.XML';
        $this->outputCsvPath = $publicPath . 'csvOutput/robeonline/articles.csv';
    }

}
