<?php

namespace App\Command\endlichSicher;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ArticleUpdateCommand extends Command {

    protected $apiClient;
    protected $articleContent;
    protected $base;
    protected $csvFileName;
    protected $dumppath;
    protected static $defaultName = 'app:endlichsicher:article:update';

    protected function configure() {
        $this
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->addOption('dumpCsv', 'd', InputOption::VALUE_OPTIONAL, 'If s available then run dump csv function', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }

        $fields = $this->configureFields();
        $articleCollection = $this->articleContent;
        $variants = $this->prepareArticle($output, $fields, $articleCollection);

        $insertedArray = $this->executePost($output, $variants);
        $this->createDump($insertedArray);
    }

    private function configureFields() {

        return [
            'order_number' => 0,
            'supplier_number' => 2,
            'ean' => 3,
            'purchase_price' => 6,
            'price' => 7, //Not sure
            'shipping_time' => 9,
        ];
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csvFileName = $publicPath . 'csvData/endlichsicher/articles.csv';
        $this->dumppath = $publicPath . 'dump/endlichsicher/';
        $this->logFileName = $publicPath . 'dump/endlichsicher/log.txt';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $url = 'https://endlich-sicher.de/stage/api/';
        $username = 'loy';
        $apiKey = 'Jx4fD6X7EWWNVq4spixdRItabWDnBkQrqKDCcspD';
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function prepareArticle($output, $fields, $articleCollection) {

        $variants = [];
        foreach ($articleCollection as $item) {

            $number = $item[$fields['order_number']];
            $ean = $item[$fields['ean']];
            $supplierNumber = $item[$fields['supplier_number']];
            $purchasePrice = $item[$fields['purchase_price']];

            $price = $item[$fields['price']];
            $priceEK = [
                'customerGroupKey' => 'EK',
                'price' => $price,
            ];

            $prices = [$priceEK];
            $shippingTime = $item[$fields['shipping_time']];

            $variantData = [
                'ean' => $ean,
                'supplierNumber' => $supplierNumber,
                'purchasePrice' => $purchasePrice,
                'prices' => $prices,
                'shippingTime' => $shippingTime,
            ];

            $variants[$number] = $variantData;
        }

        return $variants;
    }

    private function executePost($output, $variants) {

        $errorArray = [];
        $insertCount = 0;
        $insertedArray = [];

        $output->write('---------------------------', true);
        foreach ($variants as $number => $variant) {
            $status = $this->apiClient->put('variants/' . $number . '?useNumberAsId=1', $variant);
            if (!$status['success']) {
                $this->base->writeLog($number, TRUE, 'B');

                $output->write('Failed ' . $number, true);
                $errorArray[] = $number;
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $number . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
            $insertedArray[$number] = $status['data']['id'];
        }
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }

        return $insertedArray;
    }

    /**
     * Create csv of entries updated
     * @param type $insertArray
     */
    private function createDump($insertArray) {

        $file = fopen($this->dumppath . "updated-variants.csv", "w");
        foreach ($insertArray as $number => $variantId) {
            fputcsv($file, [$number, $variantId], ";");
        }
        fclose($file);
    }

}
