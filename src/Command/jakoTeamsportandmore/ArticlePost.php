<?php

namespace App\Command\jakoTeamsportandmore;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ArticlePost extends Command {

    protected $apiClient;
    protected $categoryContent;
    protected $articleContent;
    protected $base;
    protected $articleController;
    protected $csv;
    protected $csvFileName;
    protected $imageLocationLocal;
    protected $imageLocatioRemote;
    protected $category;
    protected $descriptionsTexts;
    protected $runStage;
    protected $missingImageFile;
    protected $missingImageFileArticle;
    protected $createMissingImageArticles;
    protected $outputCsvFileName;
    protected $outputCsvImageFileName;
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:jako:teamsportandmore:post-articles';

    protected function configure() {
        $this
                ->setDescription('Article post command for teamsportandmore')
                ->setHelp('teamsportandmore - post - article ');
    }

    /**
     * Initialize values for the command
     */
    private function initValues($input, $output) {

        $this->runStage = true;
        $this->createMissingImageArticles = false;
        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'amD1Nm9fCqDl4T6rLEFhLM5cas29xHS6RDzuAZ1l';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csv = [
            'article_number' => 1,
            'ean' => 2,
            'article_name' => 3,
            'color' => 5,
            'size' => 7,
            'variant_code' => 8,
            'base_price' => 9,
            'price' => 10,
            'image1' => 11,
            'gender' => 14,
            'supplier' => 0,
            'text1' => 15,
            'text2' => 16,
            'text3' => 17,
            'text4' => 18,
            'text5' => 19,
            'text6' => 20,
            'text7' => 21,
            'text8' => 22,
            'text9' => 23,
            'text10' => 24,
            'meterial1' => 25,
            'meterial2' => 26,
            'meterial3' => 27,
            'meterial4' => 28,
        ];

        $this->outputCsvImageFileName = $publicPath . 'csvOutput/jako_teamsportandmore_image_output.csv';
        $this->outputCsvFileName = $publicPath . 'csvOutput/jako_teamsportandmore_output.csv';
        $this->csvFileName = $publicPath . 'csvData/jako_teamsportandmore.csv';

        /* To find missing images */
        $this->imageLocationLocal = '/home/binu.abraham@beo.in/Documents/Works doing/teamsport_images/jako_teamsportandmore/';
        /* To get file for import */
        $this->imageLocationRemote = '/home/binu.abraham@beo.in/Documents/Works doing/teamsport_images/jako_teamsportandmore/';
        $this->imageFields = ['image1'];
        $this->missingImageFile = 'public/jako_teamsportandmore_missing.csv';
        $this->missingImageFileArticle = 'public/jako_teamsportandmore_missing_article.csv';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $this->category = 6;
        $this->descriptionsTexts = [
            'text1', 'text2', 'text3', 'text4', 'text5', 'text6', 'text7', 'text8', 'text9', 'text10',
            'meterial1', 'meterial2', 'meterial3', 'meterial4'
        ];

        if ($this->runStage) {

            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue?', false);
            if (!$helper->ask($input, $output, $question)) {
                $output->writeln([
                    'Operation terminated'
                ]);
                return;
            }
            $this->imageLocationRemote = '/home/teakpjjw/import_images/jako_teamsportandmore/';
            $url = 'https://www.teamsportandmore.de/api';
            $username = 'admin';
            $apiKey = 'bTG2fzEgcv7KZ8r45SKTxWNN06b2fd7R3CHZl5zT';
//            $this->category = 457; //stage
            $this->category = 481;
        }
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues($input, $output);
        $insertCount = 0;
        $articles = $this->prepareArticle($input, $output, $this->articleContent);
        $errorArray = [];
        $output->write('---------------------------', true);
        $file = fopen($this->outputCsvFileName, "w");
        foreach ($articles as $article) {
            $status = $this->apiClient->post('articles', $article);
            if (!$status['success']) {

                $output->write('Failed ' . $article['mainDetail']['number'], true);
                $errorArray[] = $article['mainDetail']['number'];
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $article['mainDetail']['number'] . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
            $articleId = $status['data']['id'];
            $articleName = $article['name'];
            fputcsv($file, [$articleName, $articleId], ";");
            sleep(1);
        }
        fclose($file);
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }
        die;
    }

    private function prepareArticle(InputInterface $input, OutputInterface $output, $articleCollection) {

        $set = [];
        $articlesArray = [];
        $imagesCollection = $this->importImages($this->articleContent);

        if (!$this->runStage) {
            $this->missingImages($articleCollection);
        }
        foreach ($articleCollection as $key => $item) {
            $articlesArray[$item[$this->csv['article_number']]][] = $item;
        }
        foreach ($articlesArray as $key => $variantItem) {
            $article = [];
            $colors = [];
            $colorNames = [];
            $sizes = [];
            $sizeNames = [];
            $variants = [];
            $configurationSetOptionsColour = [];
            $configurationSetOptionsSize = [];
            $description = '';
            foreach ($variantItem as $variantIndex => $item) {
                if ($variantIndex == 0) {
                    if ($item[$this->csv['gender']] != 'Unisex') {
                        $articleName = $item[$this->csv['article_name']] . ', ' . $item[$this->csv['gender']];
                    } else {
                        $articleName = $item[$this->csv['article_name']];
                    }
                    $article['name'] = $articleName;
                    $article['active'] = true;
                    if ($this->runStage) {
                        $article['supplierId'] = 4;
                    } else {
                        $article['supplier'] = 'JAKO';
                    }
                    $article['tax'] = 19;


                    foreach ($this->descriptionsTexts as $textIndex) {
                        if ($item[$this->csv[$textIndex]] != '') {
                            $description .= '<p>' . $item[$this->csv[$textIndex]] . '</p>';
                        }
                    }
                    $article['descriptionLong'] = $description;
                }

                $article['categories'] = [
                    ['id' => $this->category]
                ];

                $colourCode = ucwords($item[$this->csv['color']]);
                if (!in_array($colourCode, $colorNames)) {
                    array_push($colors, ['name' => $colourCode]);
                    array_push($colorNames, $colourCode);
                    $configurationSetOptionsColour[] = ['name' => $colourCode];
                }

                $sizeCode = $item[$this->csv['size']];
                if (!in_array($sizeCode, $sizeNames)) {
                    array_push($sizes, ['name' => $sizeCode]);
                    array_push($sizeNames, $sizeCode);
                    $configurationSetOptionsSize[] = ['name' => $sizeCode];
                }

                $images = [];
                if ($item[$this->csv['image1']] != '' && $imagesCollection[$item[$this->csv['image1']]] != '') {
                    $images[] = [
                        'mediaId' => $imagesCollection[$item[$this->csv['image1']]],
                        'options' => [
                            [
                                [
                                    'name' => $colourCode,
                                ]
                            ]
                        ]
                    ];
                }

                $variant = [];
                $variant['ean'] = $item[$this->csv['ean']];
                $variant['isMain'] = false;
                $variant['inStock'] = 1;
                $variant['number'] = $item[$this->csv['article_number']] . '-' . $item[$this->csv['variant_code']];
                $variant['active'] = TRUE;
                $variant['shippingTime'] = '3-5';

                $priceEK = [
                    'customerGroupKey' => 'EK',
                    'price' => $item[$this->csv['price']],
                    'basePrice' => $item[$this->csv['base_price']]
                ];
                $priceH = [
                    'customerGroupKey' => 'H',
                    'price' => $item[$this->csv['price']],
                    'basePrice' => $item[$this->csv['base_price']]
                ];

                $prices = [$priceEK, $priceH];
                $variant['prices'] = $prices;
                $configurationSet = [
                    'type' => 2,
                    'groups' => [
                        [
                            'name' => 'Farbe',
                            'options' => $configurationSetOptionsColour
                        ],
                        [
                            'name' => 'Größe',
                            'options' => $configurationSetOptionsSize
                        ]
                    ]
                ];
                $variant['configuratorOptions'] = [
                    ['group' => 'Farbe', 'option' => $colourCode],
                    ['group' => 'Größe', 'option' => $sizeCode]
                ];
                if (count($images) > 0) {
                    $variant['images'] = $images;
                }
                $variants[] = $variant;
            }

            $variants[0]['isMain'] = true;

            $mainDetail = $variants[0];
            $article['mainDetail'] = $mainDetail;
            array_splice($variants, 0, 1);
            $article['configuratorSet'] = $configurationSet;
            if (count($variants) > 0) {
                $article['variants'] = $variants;
            }

//            $status = $this->apiClient->post('articles', $article);
//            dump($status);
//            die;
            $set[] = $article;
        }
        $output->writeln([
            'Article data prepared.',
            'count of articles ' . count($set),
            'total variants ' . count($articleCollection),
        ]);
        return $set;
    }

    /**
     * Get images names from csv which are not available in the image directory
     * As per max, we insert those articles without images
     */
    private function missingImages($articleCollection) {

        $missingArray = [];
        foreach ($articleCollection as $item) {
            $image = $item[$this->csv['image1']];
            $exist = file_exists($this->imageLocationLocal . $image);
            if (!$exist && !in_array($image, $missingArray)) {
                $missingArray[] = $image;
            }
        }
        if (count($missingArray) > 0) {
            $file = fopen($this->missingImageFile, "w");
            foreach ($missingArray as $image) {
                fputcsv($file, [$image]);
            }
            fclose($file);
        }

        if ($this->createMissingImageArticles) {
            $file = fopen($this->missingImageFileArticle, "w");
            $articleName = [];
            foreach ($missingArray as $image) {
                $splittedImageName = explode('_', $image);
                if (!in_array($splittedImageName[0], $articleName)) {
                    $articleName[] = $splittedImageName[0];
                    fputcsv($file, [$splittedImageName[0]]);
                }
            }
            fclose($file);
        }



        return $missingArray;
    }

    private function importImages($articles) {

        $imageCollection = [];
        $file = fopen($this->outputCsvImageFileName, "w");
        foreach ($articles as $article) {
            if ($article[$this->csv['image1']] != '' && !in_array($article[$this->csv['image1']], array_keys($imageCollection))) {
                $name = explode('.', $article[$this->csv['image1']]);
                $mediaData = [
                    'album' => -1,
                    'file' => $this->imageLocationRemote . $article[$this->csv['image1']],
                    'name' => $name[0],
                    'description' => $name[0]
                ];
                $status = $this->apiClient->post('media', $mediaData);
                $imageCollection[$article[$this->csv['image1']]] = $status['data']['id'];
                fputcsv($file, [$article[$this->csv['image1']], $status['data']['id']], ";");
                sleep(1);
            }
        }
        fclose($file);
        return $imageCollection;
    }

}
