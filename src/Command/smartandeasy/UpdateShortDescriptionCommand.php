<?php

namespace App\Command\smartandeasy;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class UpdateShortDescriptionCommand extends Command {

    protected $stage;
    protected $apiClient;
    protected $articleContent;
    protected $base;
    protected $csvFileName;
    protected $missingImageFile;
    protected $category;
    protected $outputCsvFileName;
    protected $dumpCsv;
    protected $catmap;
    protected $categoryIdList;
    protected $rewriteUrls;
    protected $subShopId;
    protected $missingImages;
    protected static $defaultName = 'app:smartandeasy:article-update-short-description';

    protected function configure() {
        $this
                ->setDescription('Create article - smartandeast')
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->setHelp('Update article description load images from description to database and show it feom the database itself');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }

        $fields = $this->configureFields();
        $articleCollection = $this->articleContent;

        $articles = $this->prepareArticle($output, $fields, $articleCollection);
        die('Done');
    }

    private function configureFields() {

        return [
            'super_product' => 55,
            'order_number' => 1,
            'active' => 3,
            'image' => 5,
            'more_images' => 66,
            'pseudo_sales' => 6,
            'price' => 6, // + 19%
            'weight' => 28,
            'supplier_name' => 21,
            'supplier_number' => 22,
            'name' => 33,
            'name_eng' => 34,
            'meta_title' => 35,
            'meta_title_eng' => 36,
            'rewrite_url' => 37,
            'rewrite_url_eng' => 38,
            'description' => 39, //strip html
            'description_eng' => 40, //strip html
            'description_long' => 41, //strip h1
            'description_long_eng' => 42, //strip h1
            'ean' => 23,
            'shipping_time' => 26,
            'keywords' => 43,
            'keywords_eng' => 44,
            'available_from' => 50,
            'config_group' => 56,
        ];
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csvFileName = $publicPath . 'csvData/smartandeasy/articles.csv';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $this->subShopId = 6;
        $this->apiClient = $this->base->getClient($url, $username, $apiKey, false);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $url = 'http://smartandeasy.toller-testserver2.de/api';
        $username = 'loyadmin';
        $apiKey = 'sT4pVsXaLQU5GrZ1MmEq7qHbWmBOTK9kDIbG9LLb';
        $this->subShopId = 3;

        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function prepareArticle(OutputInterface $output, $fields, $articleCollection) {

        $articlesArray = [];
        foreach ($articleCollection as $key => $item) {
            if ($item[$fields['name']] == '' && $item[$fields['name_eng']] == '') {
                unset($articleCollection[$key]);
            } else {
                if ($item[$fields['super_product']] != '') {
                    $articlesArray[$item[$fields['super_product']]] = $item;
                } else {
                    $articlesArray[$item[$fields['order_number']]] = $item;
                }
            }
        }

        foreach ($articlesArray as $key => $articleItem) {

            $number = $this->convertOrderNumbers($articleItem[$fields['order_number']]);
            $description = $articleItem[$fields['description']];
            $descriptionEng = $articleItem[$fields['description_eng']];

            $translations = [
                'shopId' => $this->subShopId,
                'description' => $descriptionEng,
            ];

            $updateContent = [
                'description' => $description,
                'translations' => [$translations],
            ];
            $status = $this->apiClient->put('articles/' . $number . '?useNumberAsId=true', $updateContent);
            dump(($status['success'] ? 'Succeed ' : 'Failed ') . $number);
        }
    }

    private function convertOrderNumbers($orderNumber) {
        return preg_replace("/[^a-zA-Z0-9-_.]/", "", $orderNumber);
    }

}
