<?php

namespace App\Command\smartandeasy;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class CategoryCommand extends Command {

    protected $stage;
    protected $apiClient;
    protected $categoryContent;
    protected $base;
    protected $dumpCsv;
    protected $insertArray;
    protected $insertedArrayFile;
    protected $counting;
    protected $parentMissing;
    private $subShopId;
    private $baseCatId;
    private $missingImages;
    protected static $defaultName = 'app:smartandeasy:category';

    protected function configure() {
        $this
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->addOption('dumpCsv', 'd', InputOption::VALUE_OPTIONAL, 'If s available then run dump csv function', false)
                ->setDescription('Smart and easy category')
                ->setHelp('Smart and easy category import');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->initValues();
        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }

        $fields = $this->configureFields();
        $categoryCollection = $this->categoryContent;

        $descImageUrlCollection = $this->getImagesFromDesc($categoryCollection);
        $categoryCollection = $this->getUpdatedDescriptions($descImageUrlCollection, $categoryCollection);


        $categories = $this->prepareCategory($output, $fields, $categoryCollection);

        $this->createDump($output);
        dump($this->missingImages);
    }

    private function configureFields() {

        return [
            'type_class' => 0, //                                               //used
            'parent' => 1, //                                                   //used
            'alias' => 2,
            'position' => 3, //                                                 //used
            'active' => 4, //true false                                         //used
            'hide_top' => 5, //true false                                       //used
            'name_ger' => 6, //                                                 //used
            'name_eng' => 7, //                                                 //used
            'meta_title_ger' => 8, //                                           //used
            'meta_title_eng' => 9, //                                           //used
            's_core_rewrite_gen' => 10,
            's_core_rewrite_eng' => 11,
            'meta_description_ger' => 14, //strip html                          //used
            'meta_description_eng' => 15, //strip html                          //used
            'meta_keywords_ger' => 18, //                                       //used
            'meta_keywords_eng' => 19, //                                        //used
            'cms_text_ger' => [16, 23], //                                      //used
            'cms_text_eng' => [17, 24], //                                      //used
        ];
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $publicPath = __DIR__ . "/../../../public/";
        $this->csvFileName = $publicPath . 'csvData/smartandeasy/categories.csv';
        $this->insertedArrayFile = $publicPath . 'csvOutput/smartandeasy/categories.csv';
        $this->base = new BaseController();
        $this->categoryContent = $this->getCategoiesOnly();
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
        $this->subShopId = 6;
        $this->baseCatId = 3;
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $url = 'http://smartandeasy.toller-testserver2.de/api';
        $username = 'loyadmin';
        $apiKey = 'sT4pVsXaLQU5GrZ1MmEq7qHbWmBOTK9kDIbG9LLb';
        $this->subShopId = 3;
        $this->baseCatId = 3;
        $publicPath = __DIR__ . "/../../../public/";
        $this->insertedArrayFile = $publicPath . 'csvOutput/smartandeasy/stage/categories.csv';
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function prepareCategory(OutputInterface $output, $fields, $categoryCollection) {

        $insertCount = 0;
        $errorArray = [];
        foreach ($categoryCollection as $key => $categoryData) {


            $category = [];
            $parentString = $categoryData[$fields['parent']];
            if ($parentString != '') {
                $parentCategoryInfo = $this->getParentCategory($categoryData);
                $parentId = $parentCategoryInfo['id'];
            } else {
                $parentId = $this->baseCatId;
            }

            $categoryName = $categoryData[$fields['name_ger']] != '' ? $categoryData[$fields['name_ger']] : $categoryData[$fields['alias']];
            $category['name'] = $categoryName;
            $category['parentId'] = $parentId;
            $category['position'] = $categoryData[$fields['position']];
            $category['active'] = $categoryData[$fields['active']] == 1 ? true : false;
            $category['hideTop'] = $categoryData[$fields['active']] == 1 ? true : false;
            $category['metaTitle'] = $categoryData[$fields['meta_title_ger']];
            $category['metaDescription'] = strip_tags($categoryData[$fields['meta_description_ger']]);
            $category['metaKeywords'] = $categoryData[$fields['meta_keywords_ger']];
            $category['cmsHeadline'] = $categoryName;
            $category['cmsText'] = $categoryData['changed_cms_text'];


            $translations = [
                'shopId' => $this->subShopId,
                'description' => $categoryData[$fields['name_eng']],
                'metatitle' => $categoryData[$fields['meta_title_eng']],
                'metadescription' => strip_tags($categoryData[$fields['meta_description_eng']]),
                'metakeywords' => $categoryData[$fields['meta_keywords_eng']],
                'cmsheadline' => $categoryData[$fields['name_eng']],
                'cmstext' => $categoryData['changed_cms_text_eng']
            ];

            $category['translations'][] = $translations;

            $status = $this->apiClient->post('categories', $category);

            $categoryMainUrl = $categoryData[$fields['s_core_rewrite_gen']];
            $categoryEnglishUrl = $categoryData[$fields['s_core_rewrite_eng']];
            if (!$status['success']) {

                $output->write('Failed ' . $category['name'], true);
                $errorArray[] = $category['name'];
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $category['name'] . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }

            $newCategoryId = $status['data']['id'];
            if ($parentString == '') {
                $categoryFullName = $categoryData[$fields['alias']];
                $this->insertArray[$categoryFullName] = ['id' => $newCategoryId, 'urlMain' => $categoryMainUrl, 'urlEnglish' => $categoryEnglishUrl];
            }
            $this->createParentCategory($categoryData, $newCategoryId, $categoryMainUrl, $categoryEnglishUrl);
        }

        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }
        return $this->insertArray;
    }

    private function createDump($output) {

        $file = fopen($this->insertedArrayFile, "w");
        foreach ($this->insertArray as $categoryName => $value) {
            fputcsv($file, [$categoryName, $value['id'], $value['urlMain'], $value['urlEnglish']], ';');
        }
        fclose($file);
        $output->writeln('Dump created at ' . $this->insertedArrayFile);
        /*         * ******************************************************************************** */
    }

    /**
     * Remove rows which are not categories
     */
    private function getCategoiesOnly() {

        $fields = $this->configureFields();
        $categoryContent = $this->base->readCSV($this->csvFileName);
        $whiteList = ['StartPage', 'Category'];
        foreach ($categoryContent as $category) {
            if (in_array($category[$fields['type_class']], $whiteList)) {
                $categories[] = $category;
            }
        }

        return $categories;
    }

    /**
     * Get parent category of selected item
     */
    private function getParentCategory($category) {

        $fields = $this->configureFields();
        $parentString = $category[$fields['parent']];
        if (isset($this->insertArray[$parentString])) {
            return $this->insertArray[$parentString];
        } {
            $this->parentMissing[] = $parentString;
        }
    }

    private function createParentCategory($category, $newCategoryId, $categoryMainUrl, $categoryEnglishUrl) {

        $fields = $this->configureFields();
        $parentString = $category[$fields['parent']];
        $aliasString = $category[$fields['alias']];
        if (strpos($aliasString, ' ') !== false) {
            $aliasString = '"' . $aliasString . '"';
        }
        if (strpos($aliasString, '/') !== false) {
            $aliasString = '"' . $aliasString . '"';
        }
        $fullCategoryPath = $parentString != '' ? $parentString . '/' . $aliasString : $aliasString;
        $this->insertArray[$fullCategoryPath] = ['id' => $newCategoryId, 'urlMain' => $categoryMainUrl, 'urlEnglish' => $categoryEnglishUrl];
    }

    /*     * **
     * Importing images
     */

    private function getImagesFromDesc($categoryArray) {
        $oldToNewUrls = [];
        $fields = $this->configureFields();
        foreach ($categoryArray as $key => $categoryItem) {
            $longDesc = $this->base->combineFields($categoryItem, $fields['cms_text_ger'], FALSE);
            //// Add code to select meta_description_ger value if cmstext is null
            if (trim($longDesc) == '') {
                $longDesc = $categoryItem[$fields['meta_description_ger']];
            }


            $images = $this->fetchingImages($longDesc);
            foreach ($images as $old) {
                if (!isset($oldToNewUrls[$old])) {
                    $newUrl = $this->uploadImage($old);
                    if ($newUrl) {
                        $oldToNewUrls[$old] = $newUrl;
                    }
                }
            }
        }
        return $oldToNewUrls;
    }

    private function fetchingImages($str) {

        $doc = new \DOMDocument();
        @$doc->loadHTML($str);

        $imageUrls = [];
        $tags = $doc->getElementsByTagName('img');
        foreach ($tags as $key => $tag) {
            $imageUrls[] = $tag->getAttribute('src');
        }
        return $imageUrls;
    }

    private function uploadImage($image) {

        if (!strpos($image, 'www.bdt-dental.de') !== false) {
            $name = str_replace(['https://', 'http://'], '', $image);
            $name = str_replace(['.', '-'], '-', $name);

            $substr = substr($image, 0, 8);
            if ($substr === '/WebRoot') {
                $image = 'https://www.smartandeasy.net' . $image;
            }

            $mediaInfo = [
                'album' => -1,
                'file' => $image,
                'name' => $name,
                'description' => $name
            ];
            $status = $this->apiClient->post('media', $mediaInfo);
            if (is_array($status)) {
                $id = $status['data']['id'];
                $path = $this->getNewPath($id);
                return $path;
            } else {
                $this->missingImages[] = $image;
            }
            return NULL;
        }
        return NULL;
    }

    private function getNewPath($id) {

        if ($this->stage !== false) {
            $username = 'loyadmin';
            $apiKey = 'sT4pVsXaLQU5GrZ1MmEq7qHbWmBOTK9kDIbG9LLb';
            $mediaUrl = "http://smartandeasy.toller-testserver2.de/api/media/" . $id;
            $removeUrl = "http://smartandeasy.toller-testserver2.de";
        } else {
            $username = 'loyadmin';
            $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
            $mediaUrl = "http://localhost/teamsportandmore/api/media/" . $id;
            $removeUrl = "http://localhost";
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $mediaUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $apiKey);
        $result = curl_exec($ch);
        curl_close($ch);
        $insertedImage = json_decode($result, TRUE);
        $path = $insertedImage['data']['path'];
        $removeDomainPath = str_replace($removeUrl, '', $path);
        return $removeDomainPath;
    }

    private function getUpdatedDescriptions($oldToNewUrls, $categoryArray) {

        $fields = $this->configureFields();
        foreach ($categoryArray as $key => $categoryItem) {


            $cmsText = $this->base->combineFields($categoryItem, $fields['cms_text_ger'], FALSE);
            if (trim($cmsText) == '') {
                $cmsText = $categoryItem[$fields['meta_description_ger']];
            }

            $cmsTextEng = $this->base->combineFields($categoryItem, $fields['cms_text_eng'], FALSE);
            if (trim($cmsTextEng) == '') {
                $cmsTextEng = $categoryItem[$fields['meta_description_eng']];
            }

            $images = $this->fetchingImages($cmsText);
            $changedCmsText = $this->updateDesc($cmsText, $images, $oldToNewUrls);
            $changedCmsTextEng = $this->updateDesc($cmsTextEng, $images, $oldToNewUrls);



            $categoryArray[$key]['changed_cms_text'] = $this->removeInlineStyle($changedCmsText);
            $categoryArray[$key]['changed_cms_text_eng'] = $this->removeInlineStyle($changedCmsTextEng);
        }

        return $categoryArray;
    }

    private function updateDesc($longDesc, $images, $oldToNewUrls) {

        foreach ($images as $image) {
            if (isset($oldToNewUrls[$image])) {
                $longDesc = str_replace($image, $oldToNewUrls[$image], $longDesc);
            }
        }
        return $longDesc;
    }

    private function removeInlineStyle($str) {

        $changedString = preg_replace('/style=\\"[^\\"]*\\"/', '', $str);

        return $changedString;
    }

}
