<?php

namespace App\Command\smartandeasy;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class UpdateArticleDescriptionStyle extends Command {

    protected $stage;
    protected $apiClient;
    protected $base;
    protected $articles;
    protected $subShopId;
    protected static $defaultName = 'app:smartandeasy:article-update-remove-inline-style';

    protected function configure() {
        $this
                ->setDescription('Create article - smartandeast')
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->addOption('dumpCsv', 'd', InputOption::VALUE_OPTIONAL, 'If s available then run dump csv function', false)
                ->setHelp('Remove inline style from article description and remove <h1> tag from the content');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }

        foreach ($this->articles as $article) {

            $articleDescription = '';
            $articleDescriptionEng = '';
            $articleInfo = $this->apiClient->get('articles/' . $article);
            $articleDescription = $articleInfo['data']['descriptionLong'];
            if (isset($articleInfo['data']['translations']['descriptionLong'])) {
                $articleDescriptionEng = $articleInfo['data']['translations']['descriptionLong'];
            }

            $updatedDescription = $this->updateDescriptionStyles($articleDescription);
            $updatedDescriptionEng = $this->updateDescriptionStyles($articleDescriptionEng);

            $updateInfo = [
                'descriptionLong' => $updatedDescription,
            ];
            $translations = [
                'shopId' => $this->subShopId,
                'descriptionLong' => $updatedDescriptionEng,
            ];
            $updateInfo['translations'][] = $translations;

            $status = $this->apiClient->put('articles/' . $article, $updateInfo);
            if ($status['success']) {
                dump('Updated . ' . $articleInfo['data']['mainDetail']['number']);
            } else {
                dump('Failed update . ' . $articleInfo['data']['mainDetail']['number']);
            }
        }
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $this->base = new BaseController();
        $this->subShopId = 6;
        $this->articles = range(104, 104);
        $this->apiClient = $this->base->getClient($url, $username, $apiKey, false);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $url = 'http://smartandeasy.toller-testserver2.de/api';
        $username = 'loyadmin';
        $apiKey = 'sT4pVsXaLQU5GrZ1MmEq7qHbWmBOTK9kDIbG9LLb';
        $this->articles = range(2086, 2523);
        $this->subShopId = 3;
        $this->apiClient = $this->base->getClient($url, $username, $apiKey, false);
    }

    private function updateDescriptionStyles($str) {

        $changedString = preg_replace('/style=\\"[^\\"]*\\"/', '', $str);
        $removedHeader = str_replace(['<h1>', '</h1>'], '', $changedString);

        return $removedHeader;
    }

}
