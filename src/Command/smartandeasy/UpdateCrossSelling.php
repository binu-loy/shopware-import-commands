<?php

namespace App\Command\smartandeasy;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class UpdateCrossSelling extends Command {

    protected $stage;
    protected $apiClient;
    protected $articleContent;
    protected $base;
    protected $csvFileName;
    protected $dumpCsv;
    protected $articleIdArray;
    protected $ignoredRows;
    protected static $defaultName = 'app:smartandeasy:cross-selling-update';

    protected function configure() {
        $this
                ->setDescription('Update article for cross selling - smartandeast')
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->setHelp('Create article - smartandeasy');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }

        $fields = $this->configureFields();
        $articleCollection = $this->articleContent;

        $articles = $this->prepareArticle($output, $fields, $articleCollection);
        $insertedArray = $this->executePost($output, $articles);

//        $this->createDumpInsertedArray($insertedArray);
    }

    private function configureFields() {

        return [
            'action' => 0,
            'main_number' => 1,
            'relating' => 2
        ];
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csvFileName = $publicPath . 'csvData/smartandeasy/cross-selling.csv';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $this->apiClient = $this->base->getClient($url, $username, $apiKey, false);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $url = 'http://smartandeasy.toller-testserver2.de/api';
        $username = 'loyadmin';
        $apiKey = 'sT4pVsXaLQU5GrZ1MmEq7qHbWmBOTK9kDIbG9LLb';

        $this->apiClient = $this->base->getClient($url, $username, $apiKey, false);
    }

    private function prepareArticle(OutputInterface $output, $fields, $articleCollection) {

        $articles = [];
        foreach ($articleCollection as $key => $item) {

            $mainNumber = $this->convertOrderNumbers($item[$fields['main_number']]);
            $articles[$mainNumber][] = $item;
        }

        $articleSet = [];
        foreach ($articles as $key => $article) {
            $articleInfo = [];
            $related = [];
            $similar = [];
            foreach ($article as $item) {
                $action = strtolower($item[$fields['action']]);
                $mainNumber = $this->convertOrderNumbers($item[$fields['main_number']]);
                $relating = $this->convertOrderNumbers($item[$fields['relating']]);

                $articleId = $this->getArticleFromNumber($mainNumber);
                $relatingId = $this->getArticleFromNumber($relating);

                if ($relatingId != '') {
                    if ($action === 'crossselling') {
                        $similar[] = ['id' => $relatingId];
                    } elseif ($action === 'accessory') {
                        $related[] = ['id' => $relatingId];
                    } else {
                        $this->ignoredRows[] = ['number' => $mainNumber, 'relating' => $relating, 'msg' => $action];
                    }
                    if (count($related) > 0) {
                        $articleInfo['related'] = $related;
                    }
                    if (count($similar) > 0) {
                        $articleInfo['similar'] = $similar;
                    }
                }
            }
            $articleSet[$key] = $articleInfo;
        }
        return $articleSet;
    }

    private function executePost($output, $articles) {
        $errorArray = [];
        $insertCount = 0;
        $insertedArray = [];


        $output->write('---------------------------', true);
        foreach ($articles as $number => $articleInfo) {
            $status = $this->apiClient->put('articles/' . $number . '?useNumberAsId=true', $articleInfo);
            if (!$status['success']) {

                $output->write('Failed ' . $number, true);
                $errorArray[] = $number;
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $number . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
            sleep(1);
        }
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }
        $this->createDump();

        return $insertedArray;
    }

    private function createDump() {

        $dumpPath = __DIR__ . "/../../../public/dump/smartandeasy/";

        /** Images inserted with ids * */
        $file = fopen($dumpPath . "ignored_rows.csv", "w");
        foreach ($this->ignoredRows as $values) {
            fputcsv($file, $values, ";");
        }
        fclose($file);
        /*         * ***************************************************************************************************** */
    }

    private function getArticleFromNumber($number) {

        $articleId = '';
        if (isset($this->articleIdArray[$number])) {
            $articleId = $this->articleIdArray[$number];
        } else {
            $article = $this->apiClient->get('articles/' . $number . '?useNumberAsId=true');
            if (!isset($article['success'])) {
                $this->ignoredRows[] = ['number' => $number, 'nothing'=> '','msg' => 'Couldn\'t found number'];
            } else {
                $articleId = $article['data']['id'];
                $this->articleIdArray[$number] = $articleId;
            }
        }
        return $articleId;
    }

    private function convertOrderNumbers($orderNumber) {
        return preg_replace("/[^a-zA-Z0-9-_.]/", "", $orderNumber);
    }

}
