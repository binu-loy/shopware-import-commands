<?php

namespace App\Command\smartandeasy;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class UpdateDescription extends Command {

    protected $stage;
    protected $apiClient;
    protected $articleContent;
    protected $base;
    protected $csvFileName;
    protected $missingImageFile;
    protected $category;
    protected $outputCsvFileName;
    protected $dumpCsv;
    protected $catmap;
    protected $categoryIdList;
    protected $rewriteUrls;
    protected $subShopId;
    protected $missingImages;
    protected static $defaultName = 'app:smartandeasy:article-update';

    protected function configure() {
        $this
                ->setDescription('Create article - smartandeast')
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->addOption('dumpCsv', 'd', InputOption::VALUE_OPTIONAL, 'If s available then run dump csv function', false)
                ->setHelp('Update article description load images from description to database and show it feom the database itself');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }

        $fields = $this->configureFields();
        $articleCollection = $this->articleContent;

        $articles = $this->prepareArticle($output, $fields, $articleCollection);

        echo 'Following images are missing or couldn\'t fetch';
        dump($this->missingImages);
        die;
    }

    private function configureFields() {

        return [
            'super_product' => 55,
            'order_number' => 1,
            'active' => 3,
            'image' => 5,
            'more_images' => 66,
            'pseudo_sales' => 6,
            'price' => 6, // + 19%
            'weight' => 28,
            'supplier_name' => 21,
            'supplier_number' => 22,
            'name' => 33,
            'name_eng' => 34,
            'meta_title' => 35,
            'meta_title_eng' => 36,
            'rewrite_url' => 37,
            'rewrite_url_eng' => 38,
            'description' => 39, //strip html
            'description_eng' => 40, //strip html
            'description_long' => 41, //strip h1
            'description_long_eng' => 42, //strip h1
            'ean' => 23,
            'shipping_time' => 26,
            'keywords' => 43,
            'keywords_eng' => 44,
            'available_from' => 50,
            'config_group' => 56,
        ];
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csvFileName = $publicPath . 'csvData/smartandeasy/articles.csv';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $catmapList = $this->base->readCSV($publicPath . 'csvData/smartandeasy/cat-art-map.csv');
        $this->catmap = array_combine(array_column($catmapList, 1), $catmapList);
        $categoryIdList = $this->base->readCSV($publicPath . 'csvOutput/smartandeasy/categories.csv');
        $this->categoryIdList = array_combine(array_column($categoryIdList, 0), array_column($categoryIdList, 1));
        $this->category = 3;
        $this->subShopId = 3;
        $this->apiClient = $this->base->getClient($url, $username, $apiKey, false);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $url = 'http://smartandeasy.toller-testserver2.de/api';
        $username = 'loyadmin';
        $apiKey = 'sT4pVsXaLQU5GrZ1MmEq7qHbWmBOTK9kDIbG9LLb';
        $this->category = 3;
        $this->subShopId = 3;
        $publicPath = __DIR__ . "/../../../public/";
        $categoryIdList = $this->base->readCSV($publicPath . 'csvOutput/smartandeasy/stage/categories.csv');
        $this->categoryIdList = array_combine(array_column($categoryIdList, 0), array_column($categoryIdList, 1));

        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function prepareArticle(OutputInterface $output, $fields, $articleCollection) {

        $articlesArray = [];
        foreach ($articleCollection as $key => $item) {
            if ($item[$fields['name']] == '' && $item[$fields['name_eng']] == '') {
                unset($articleCollection[$key]);
            } else {
                if ($item[$fields['super_product']] != '') {
                    $articlesArray[$item[$fields['super_product']]] = $item;
                } else {
                    $articlesArray[$item[$fields['order_number']]] = $item;
                }
            }
        }

        $oldToNewUrls = [];
        $urlCount = 1;
        foreach ($articlesArray as $key => $articleItem) {

            $longDesc = $articleItem[$fields['description_long']];
            $images = $this->fetchingImages($longDesc);
            foreach ($images as $old) {
                if (!isset($oldToNewUrls[$old])) {
                    $newUrl = $this->uploadImage($old);
                    if ($newUrl) {

                        dump('receiving new urls  ' . $urlCount);
                        $oldToNewUrls[$old] = $newUrl;
                        $urlCount = $urlCount + 1;
                    }
                }
            }
        }
        foreach ($articlesArray as $key => $articleItem) {

            $number = $this->convertOrderNumbers($articleItem[$fields['order_number']]);
            $longDesc = $articleItem[$fields['description_long']];
            $longDescEng = $articleItem[$fields['description_long_eng']];

            $images = $this->fetchingImages($longDesc);
            $changedLongDesc = $this->updateDesc($longDesc, $images, $oldToNewUrls);
            $changedLongDescEng = $this->updateDesc($longDescEng, $images, $oldToNewUrls);

            $translations = [
                'shopId' => $this->subShopId,
                'descriptionLong' => $changedLongDescEng,
            ];

            $updateContent = [
                'descriptionLong' => $changedLongDesc,
                'translations' => [$translations],
            ];
            $this->apiClient->put('articles/' . $number . '?useNumberAsId=true', $updateContent);
        }
    }

    private function fetchingImages($str) {

        $imageUrls = [];
        $doc = new \DOMDocument();
        @$doc->loadHTML($str);

        $tags = $doc->getElementsByTagName('img');
        foreach ($tags as $key => $tag) {
            $imageUrls[] = $tag->getAttribute('src');
        }
        return $imageUrls;
    }

    private function uploadImage($image) {

        if (!strpos($image, 'www.bdt-dental.de') !== false) {       //these files cannot receive, permission issue
            $name = str_replace(['https://', 'http://'], '', $image);
            $name = str_replace(['.', '-'], '-', $name);

            $substr = substr($image, 0, 8);
            if ($substr === '/WebRoot') {
                $image = 'https://www.smartandeasy.net' . $image;
            }

            $mediaInfo = [
                'album' => -1,
                'file' => $image,
                'name' => $name,
                'description' => $name
            ];
            $status = $this->apiClient->post('media', $mediaInfo);
            if (is_array($status)) {
                $id = $status['data']['id'];
                $path = $this->getNewPath($id);
                return $path;
            } else {
                $this->missingImages[] = $image;
            }
            return NULL;
        }
        return NULL;
    }

    private function updateDesc($longDesc, $images, $oldToNewUrls) {

        foreach ($images as $image) {
            if (isset($oldToNewUrls[$image])) {
                $longDesc = str_replace($image, $oldToNewUrls[$image], $longDesc);
            }
        }
        return $longDesc;
    }

    private function getNewPath($id) {

        if ($this->stage !== false) {
            $username = 'loyadmin';
            $apiKey = 'sT4pVsXaLQU5GrZ1MmEq7qHbWmBOTK9kDIbG9LLb';
            $mediaUrl = "http://smartandeasy.toller-testserver2.de/api/media/" . $id;
            $removeUrl = "http://smartandeasy.toller-testserver2.de";
        } else {
            $username = 'loyadmin';
            $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
            $mediaUrl = "http://localhost/teamsportandmore/api/media/" . $id;
            $removeUrl = "http://localhost";
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $mediaUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $apiKey);
        $result = curl_exec($ch);
        curl_close($ch);
        $insertedImage = json_decode($result, TRUE);
        $path = $insertedImage['data']['path'];
        $removeDomainPath = str_replace($removeUrl, '', $path);
        return $removeDomainPath;
    }

    private function convertOrderNumbers($orderNumber) {
        return preg_replace("/[^a-zA-Z0-9-_.]/", "", $orderNumber);
    }

}
