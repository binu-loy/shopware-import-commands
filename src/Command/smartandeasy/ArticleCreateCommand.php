<?php

namespace App\Command\smartandeasy;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Controller\BaseController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ArticleCreateCommand extends Command {

    protected $stage;
    protected $apiClient;
    protected $articleContent;
    protected $base;
    protected $csvFileName;
    protected $missingImageFile;
    protected $category;
    protected $outputCsvFileName;
    protected $dumpCsv;
    protected $catmap;
    protected $categoryIdList;
    protected $rewriteUrls;
    protected $subShopId;
    protected $offerCSV;
    protected $offerPrices;
    protected static $defaultName = 'app:smartandeasy:article-create';

    protected function configure() {
        $this
                ->setDescription('Create article - smartandeast')
                ->addOption('stage', 's', InputOption::VALUE_OPTIONAL, 'If s available then run on stage', false)
                ->addOption('dumpCsv', 'd', InputOption::VALUE_OPTIONAL, 'If s available then run dump csv function', false)
                ->setHelp('Create article - smartandeasy');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->initValues();

        $this->stage = $input->getOption('stage');
        if ($this->stage !== false) {
            $this->initStage($input, $output);
        }

        $fields = $this->configureFields();
        $readedArticles = $this->articleContent;
        $articleCollection = $this->updateArticleCollection($readedArticles);

        $descImageUrlCollection = $this->getImagesFromDesc($articleCollection);
        $articleCollection = $this->getUpdatedDescriptions($descImageUrlCollection, $articleCollection);


        $articles = $this->prepareArticle($output, $fields, $articleCollection);
        $insertedArray = $this->executePost($output, $articles);

        $this->createDumpInsertedArray($insertedArray);
    }

    private function configureFields() {

        return [
            'super_product' => 55,
            'order_number' => 1,
            'active' => 3,
            'image' => 5,
            'more_images' => 66,
            'pseudo_sales' => 6,
            'pseudo_price' => 7,
            'price' => 6, // + 19%
            'weight' => 28,
            'supplier_name' => 21,
            'supplier_number' => 22,
            'name' => 33,
            'name_eng' => 34,
            'meta_title' => 35,
            'meta_title_eng' => 36,
            'rewrite_url' => 37,
            'rewrite_url_eng' => 38,
            'description' => 39, //strip html
            'description_eng' => 40, //strip html
            'description_long' => 41, //strip h1
            'description_long_eng' => 42, //strip h1
            'ean' => 23,
            'shipping_time' => 26,
            'keywords' => 43,
            'keywords_eng' => 44,
            'available_from' => 50,
            'config_group' => 56,
            'groups' => [
                'Farbe' => [83, 84, 85, 86],
                'Garantie' => 96,
                'Betrachtungsfläche' => 82,
                'Spannzange' => 104,
                'Stück' => 103,
                'Lieferumfang' => 100,
                '"Größe"' => 99,
                'Behälter' => 101,
                'Länge' => 102,
                'Sichtfeld' => 93,
                'kompatibel' => 88,
                'Kupplung' => 89,
                'Breite' => 81,
                '"Anschlüße"' => 80,
                'Schublade' => 98,
                'Flächen' => 79
            ]
        ];
    }

    private function initValues() {

        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $publicPath = __DIR__ . "/../../../public/";
        $this->base = new BaseController();
        $this->csvFileName = $publicPath . 'csvData/smartandeasy/articles.csv';
        $this->articleContent = $this->base->readCSV($this->csvFileName);
        $catmapList = $this->base->readCSV($publicPath . 'csvData/smartandeasy/cat-art-map.csv');
        $this->catmap = array_combine(array_column($catmapList, 1), $catmapList);
        $categoryIdList = $this->base->readCSV($publicPath . 'csvOutput/smartandeasy/categories.csv');
        $this->categoryIdList = array_combine(array_column($categoryIdList, 0), array_column($categoryIdList, 1));
        $this->category = 3;
        $this->subShopId = 6;
        $this->offerCSV = $publicPath . 'csvData/smartandeasy/offer_prices.csv';

        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function initStage($input, $output) {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('System is going to run in stage. Do you want to continue? (Y/n) ', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln([
                'Operation terminated'
            ]);
            return;
        }
        $output->writeln('<info>Starting process in stage/live</info>');

        $url = 'http://smartandeasy.toller-testserver2.de/api';
        $username = 'loyadmin';
        $apiKey = 'sT4pVsXaLQU5GrZ1MmEq7qHbWmBOTK9kDIbG9LLb';
        $this->category = 3;
        $this->subShopId = 3;
        $publicPath = __DIR__ . "/../../../public/";
        $categoryIdList = $this->base->readCSV($publicPath . 'csvOutput/smartandeasy/stage/categories.csv');
        $this->categoryIdList = array_combine(array_column($categoryIdList, 0), array_column($categoryIdList, 1));

        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    private function prepareArticle(OutputInterface $output, $fields, $articleCollection) {

        $articlesArray = [];
        $variantsArray = [];

        $articles = [];

        foreach ($articleCollection as $key => $item) {
            if ($item[$fields['name']] == '' && $item[$fields['name_eng']] == '') {
                unset($articleCollection[$key]);
            } else {
                if ($item[$fields['super_product']] != '') {
                    $variantsArray[$item[$fields['super_product']]][] = $item;
                } else {
                    $articlesArray[$item[$fields['order_number']]] = $item;
                }
            }
        }
        $this->offerPrices = $this->getOfferPrices();
        $imageCollection = $this->importImages($articleCollection);

        foreach ($articlesArray as $key => $articleItem) {

            $hasVariant = false;
            $configurationSet = [];
            if (isset($variantsArray[$key])) {

                $configurationSet = $this->getConfigurationSet($variantsArray, $key, $fields);
                $hasVariant = true;
            }

            $variants = [];
            $article = $this->getArticle($articleItem, $fields, $imageCollection, $hasVariant);
            if ($hasVariant) {
                $article['configuratorSet'] = $configurationSet;
                $articleOrderNumber = $articleItem[1];
                $variantItems = $variantsArray[$articleOrderNumber];
                $variantItems[] = $articleItem;
                $variants = $this->getVariant($variantItems, $fields, $imageCollection);
                foreach ($variants as $variantKey => $variant) {
                    if (!isset($variant['configuratorOptions'])) {
                        $variants[$variantKey]['isMain'] = true;
                        $mainDetail = $variants[$variantKey];
                    }
                }
                $article['mainDetail'] = $mainDetail;
                if (count($variants) > 0) {
                    $article['variants'] = $variants;
                }
            } else {
                $mainDetail = $this->getMainDetail($articleItem, $fields, $imageCollection);
                $article['mainDetail'] = $mainDetail;
            }

            $articles[] = $article;
        }

        $this->createDump($articleCollection, $imageCollection);

        $output->writeln([
            'Article data prepared.',
            'count of articles ' . count($articles),
            'total variants ' . count($articleCollection),
        ]);
        return $articles;
    }

    private function getConfigurationSet($variantsArray, $key, $fields) {

        $configurationSet = [];
        $confGroups = array_filter(array_unique(array_column($variantsArray[$key], $fields['config_group'])));
        if (count($confGroups) > 0) {
            $configurationSet = [];
            foreach ($confGroups as $confGroup) {
                $confGroupFields = $fields['groups'][$confGroup];
                if (is_array($confGroupFields)) {
                    $confGroupValues = [];
                    foreach ($confGroupFields as $confGroupField) {
                        $confGroupValues = array_merge($confGroupValues, array_filter(array_unique(array_column($variantsArray[$key], $confGroupField))));
                    }
                } else {
                    $confGroupValues = array_filter(array_unique(array_column($variantsArray[$key], $confGroupFields)));
                }


                $confGroupOptions = $this->getFormattedConfiguratorOptions($confGroupValues);
                $configurationSet = [
                    'groups' => [
                        [
                            'name' => $confGroup,
                            'options' => $confGroupOptions
                        ],
                    ]
                ];
            }
        }
        return $configurationSet;
    }

    private function getFormattedConfiguratorOptions($confGroupValues) {

        if (is_array($confGroupValues)) {
            $options = array_map(function($confGroupValue) {
                $capitalized = ucwords($confGroupValue);
                return ['name' => $capitalized];
            }, $confGroupValues);
        } else {
            $options = ['name' => $confGroupValues];
        }
        return $options;
    }

    private function executePost($output, $articles) {

        $errorArray = [];
        $insertCount = 0;
        $insertedArray = [];


        $output->write('---------------------------', true);
        foreach ($articles as $article) {
            $status = $this->apiClient->post('articles', $article);
            if (!$status['success']) {

                $output->write('Failed ' . $article['mainDetail']['number'], true);
                $errorArray[] = $article['mainDetail']['number'];
                $output->write("==============================================================================", true);
            } else {
                $insertCount++;
                $output->write("Success " . $article['mainDetail']['number'] . "( " . $insertCount . " )", true);
                $output->write("==============================================================================", true);
            }
            $urlMain = $this->rewriteUrls[$article['name']]['urlMain'];
            $urlEng = $this->rewriteUrls[$article['name']]['urlEng'];
            $insertedArray[$article['name']] = ['id' => $status['data']['id'], 'urlMain' => $urlMain, 'urlEng' => $urlEng];
            sleep(1);
        }
        if (count($errorArray) == 0) {
            $output->writeln(['Every entries updated successfully']);
        } else {
            $output->writeln(['Following entries need to check']);
            dump($errorArray);
        }

        return $insertedArray;
    }

    private function importImages($articles) {

        $imageCollection = [];
        $fields = $this->configureFields();
        $imageField = $fields['image'];
        $images = array_column($articles, $imageField);

        $moreImages = $fields['more_images'];
        $moreImagesColumn = array_column($articles, $moreImages);
        foreach ($moreImagesColumn as $more) {
            $images = array_merge($images, explode(';', $more));
        }
        $completeImages = array_filter(array_unique($images));

        foreach ($completeImages as $image) {
            $name = explode('/', $image);
            $mediaData = [
                'album' => -1,
                'file' => $image,
                'name' => $name[count($name) - 1],
                'description' => $name[count($name) - 1]
            ];
            $status = $this->apiClient->post('media', $mediaData);
            $imageCollection[$image] = $status['data']['id'];
        }
        return $imageCollection;
    }

    private function getArticle($item, $fields, $imageCollection, $hasVariant) {

        $orderNumber = $item[$fields['order_number']];
        if (isset($this->catmap[$orderNumber])) {
            $categoryFlow = $this->catmap[$orderNumber][0];
            $category = $this->categoryIdList[$categoryFlow];
        } else {
            $category = $this->category;  //base
        }

        $article = [];
        $article['name'] = $item[$fields['name']];
        $article['tax'] = 19;
        $article['description'] = $item[$fields['description']];
        $article['descriptionLong'] = $item[$fields['description_long']];

        $article['supplier'] = $this->getSupplier($item);
        $article['active'] = $item[$fields['active']];
        $article['pseudoSales'] = $item[$fields['pseudo_sales']];
        $article['metaTitle'] = $item[$fields['meta_title']];
        $article['keywords'] = $item[$fields['keywords']];
        $article['availableFrom'] = $item[$fields['available_from']];
        if (!$hasVariant) {
            $articleImages = [];
            if ($item[$fields['more_images']] != '') {
                $moreImages = explode(';', $item[$fields['more_images']]);
                foreach ($moreImages as $moreImage) {
                    $articleImages[] = [
                        'mediaId' => $imageCollection[$moreImage],
                    ];
                }
            }
            if (count($articleImages) == 0) {
                if ($item[$fields['image']] != '') {
                    $articleImages[] = [
                        'mediaId' => $imageCollection[$item[$fields['image']]],
                    ];
                }
            }

            if (count($articleImages) > 0) {
                $article['images'] = $articleImages;
            }
        }

        $translations = [
            'shopId' => $this->subShopId,
            'name' => $item[$fields['name_eng']],
            'metaTitle' => $item[$fields['meta_title_eng']],
            'description' => $item[$fields['description_eng']],
            'descriptionLong' => $item[$fields['description_long_eng']],
            'keywords' => $item[$fields['keywords_eng']],
        ];

        $article['translations'][] = $translations;

        $article['categories'] = [
            ['id' => $category]
        ];

        $this->rewriteUrls[$article['name']] = ['urlMain' => $item[$fields['rewrite_url']], 'urlEng' => $item[$fields['rewrite_url_eng']]];

        return $article;
    }

    private function getVariant($variantItems, $fields, $imageCollection, $mainDetail = false) {

        $variants = [];
        foreach ($variantItems as $key => $variantItem) {
            $variant = [];
            $variant['ean'] = $variantItem[$fields['ean']];
            $variant['isMain'] = false;
            $variant['inStock'] = 1;
            $orderNumber = $variantItem[$fields['order_number']];
            $variant['number'] = $this->convertOrderNumbers($orderNumber);
            $variant['active'] = $variantItem[$fields['active']];
            $variant['shippingTime'] = $variantItem[$fields['shipping_time']];
            $variant['weight'] = $variantItem[$fields['weight']];

            $prices = $this->getCompletePrices($variantItem);
            $variant['prices'] = $prices;

            if (!$mainDetail) {

                $configurationGroup = $variantItem[$fields['config_group']];
                if ($configurationGroup) {

                    $configurationFields = $fields['groups'][$configurationGroup];
                    $configurationOptions = [];
                    $configurationOptionValues = [];
                    if (is_array($configurationFields)) {
                        foreach ($configurationFields as $configurationField) {
                            if (trim($variantItem[$configurationField] != '')) {
                                $configurationOptions[] = ['group' => $configurationGroup, 'option' => $variantItem[$configurationField]];
                                $configurationOptionValues[] = $variantItem[$configurationField];
                            }
                        }
                    } else {
                        $configurationOptions[] = ['group' => $configurationGroup, 'option' => $variantItem[$configurationFields]];
                        $configurationOptionValues[] = $variantItem[$configurationFields];
                    }
                    $images = $this->getImages($variantItem, $fields, $imageCollection, $configurationOptionValues);

                    if (count($images) > 0) {
                        $variant['images'] = $images;
                    }
                    $variant['configuratorOptions'] = $configurationOptions;
                } else {
                    $images = $this->getImages($variantItem, $fields, $imageCollection, $configurationOptionValues);

                    if (count($images) > 0) {
                        $variant['images'] = $images;
                    }
                }
            }
            $variants[] = $variant;
        }
        return $variants;
    }

    private function getMainDetail($item, $fields, $imageCollection) {
        $variants = $this->getVariant([$item], $fields, $imageCollection, true);
        $mainDetail = $variants[0];
        $mainDetail['isMain'] = TRUE;
        return $mainDetail;
    }

    private function getImages($item, $fields, $imageCollection, $optionValues) {

        $images = [];
        $imageField = $fields['image'];
        $moreImageField = $fields['more_images'];
        $imageOptions = [];
        foreach ($optionValues as $optionValue) {
            $imageOptions[] = [
                [
                    'name' => $optionValue,
                ]
            ];
        }

        if ($item[$moreImageField] != '') {
            $moreImages = explode(';', $item[$moreImageField]);
            foreach ($moreImages as $moreImage) {
                if ($imageCollection[$moreImage] != '') {
                    $images[] = [
                        'mediaId' => $imageCollection[$moreImage],
                        'options' => $imageOptions,
                    ];
                }
            }
        }
        if (count($images) == 0) {
            if ($item[$imageField] != '' && $imageCollection[$item[$imageField]] != '') {
                $images[] = [
                    'mediaId' => $imageCollection[$item[$imageField]],
                    'options' => $imageOptions,
                ];
            }
        }


        return $images;
    }

    private function convertPsedoSalesToPrice($psedoSale) {

        $pseudo_sales = str_replace('.', '', $psedoSale);
        $pseudo_sales = str_replace(',', '.', $pseudo_sales);
        $intpseudo_sales = floatval($pseudo_sales);
        $tax19 = $intpseudo_sales * 0.19;
        $priceAmount = $intpseudo_sales + $tax19;
        return $priceAmount;
    }

    private function createDump($articles, $imageCollection = []) {

        $fields = $this->configureFields();
        if ($this->stage !== false) {
            $dumpPath = __DIR__ . "/../../../public/dump/smartandeasy/stage/";
        } else {
            $dumpPath = __DIR__ . "/../../../public/dump/smartandeasy/";
        }
        /** Article don't have images * */
        $file = fopen($dumpPath . "images.csv", "w");
        foreach ($articles as $article) {
            $imageField = $fields['image'];
            if ($article[$imageField] == '') {
                fputcsv($file, [$article[$fields['order_number']], 'No file inserted'], ";");
            } else {
                if ($imageCollection[$article[$imageField]] != '') {
                    fputcsv($file, [$article[$fields['order_number']], $article[$imageField], $imageCollection[$article[$imageField]]], ";");
                } else {
                    fputcsv($file, [$article[$fields['order_number']], $article[$imageField], 'File not found/imported'], ";");
                }
            }
        }
        fclose($file);
        /*         * ***************************************************************************************************** */

        /** Images inserted with ids * */
        $file = fopen($dumpPath . "image_collection.csv", "w");
        foreach ($imageCollection as $url => $imageId) {
            fputcsv($file, [$url, $imageId], ";");
        }
        fclose($file);
        /*         * ***************************************************************************************************** */
    }

    private function createDumpInsertedArray($insertedArray) {

        if ($this->stage !== false) {
            $dumpPath = __DIR__ . "/../../../public/dump/smartandeasy/stage/";
        } else {
            $dumpPath = __DIR__ . "/../../../public/dump/smartandeasy/";
        }
        $file = fopen($dumpPath . "article_inserted.csv", "w");
        foreach ($insertedArray as $article => $value) {
            fputcsv($file, [$article, $value['id'], $value['urlMain'], $value['urlEng']], ";");
        }
        fclose($file);
    }

    private function convertOrderNumbers($orderNumber) {
        return preg_replace("/[^a-zA-Z0-9-_.]/", "", $orderNumber);
    }

    private function getSupplier($item) {

        $supplierValue = '';
        $fields = $this->configureFields();
        if ($item[$fields['supplier_name']] == '') {
            $supplierValue = 'Smart´n Easy e.K.';
        } else {
            $supplierValue = $item[$fields['supplier_name']];
        }

        return $supplierValue;
    }

    private function loadImageFromCsv() {

        $publicPath = __DIR__ . "/../../../public/";
        $entriesFromCsv = $this->base->readCSV($publicPath . '/dump/smartandeasy/stage/images.csv');
        $imageCollection = array_combine(array_column($entriesFromCsv, 1), array_column($entriesFromCsv, 2));
        array_filter($imageCollection);
        return $imageCollection;
    }

    /**
     * Update article collection for fixing super product issue
     */
    private function updateArticleCollection($readedArticles) {

//        $fields = $this->configureFields();
//        $superProducts = [];
//        foreach ($readedArticles as $key => $item) {
//            if ($item[$fields['name']] == '' && $item[$fields['name_eng']] == '') {
//                unset($readedArticles[$key]);
//            } else {
//                if ($item[$fields['super_product']] != '') {
//                    $superProducts[] = $item[$fields['super_product']];
//                    $superProducts = array_unique(array_values($superProducts));
//                }
//            }
//        }
//        foreach ($readedArticles as $key => $item) {
//            $orderNumber = $item[$fields['order_number']];
//            if (in_array($orderNumber, $superProducts)) {
//                $readedArticles[$key][$fields['super_product']] = $orderNumber;
//            }
//        }
        return $readedArticles;
    }

    private function getImagesFromDesc($articlesArray) {
        $oldToNewUrls = [];
        $fields = $this->configureFields();
        foreach ($articlesArray as $key => $articleItem) {
            $longDesc = $articleItem[$fields['description_long']];
            $images = $this->fetchingImages($longDesc);
            foreach ($images as $old) {
                if (!isset($oldToNewUrls[$old])) {
                    $newUrl = $this->uploadImage($old);
                    if ($newUrl) {
                        $oldToNewUrls[$old] = $newUrl;
                    }
                }
            }
        }
        return $oldToNewUrls;
    }

    private function fetchingImages($str) {

        $doc = new \DOMDocument();
        @$doc->loadHTML($str);

        $imageUrls = [];
        $tags = $doc->getElementsByTagName('img');
        foreach ($tags as $key => $tag) {
            $imageUrls[] = $tag->getAttribute('src');
        }
        return $imageUrls;
    }

    private function uploadImage($image) {

        if (!strpos($image, 'www.bdt-dental.de') !== false) {
            $name = str_replace(['https://', 'http://'], '', $image);
            $name = str_replace(['.', '-'], '-', $name);

            $substr = substr($image, 0, 8);
            if ($substr === '/WebRoot') {
                $image = 'https://www.smartandeasy.net' . $image;
            }

            $mediaInfo = [
                'album' => -1,
                'file' => $image,
                'name' => $name,
                'description' => $name
            ];
            $status = $this->apiClient->post('media', $mediaInfo);
            if (is_array($status)) {
                $id = $status['data']['id'];
                $path = $this->getNewPath($id);
                return $path;
            } else {
                $this->missingImages[] = $image;
            }
            return NULL;
        }
        return NULL;
    }

    private function getNewPath($id) {

        if ($this->stage !== false) {
            $username = 'loyadmin';
            $apiKey = 'sT4pVsXaLQU5GrZ1MmEq7qHbWmBOTK9kDIbG9LLb';
            $mediaUrl = "http://smartandeasy.toller-testserver2.de/api/media/" . $id;
            $removeUrl = "http://smartandeasy.toller-testserver2.de";
        } else {
            $username = 'loyadmin';
            $apiKey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
            $mediaUrl = "http://localhost/teamsportandmore/api/media/" . $id;
            $removeUrl = "http://localhost";
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $mediaUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $apiKey);
        $result = curl_exec($ch);
        curl_close($ch);
        $insertedImage = json_decode($result, TRUE);
        $path = $insertedImage['data']['path'];
        $removeDomainPath = str_replace($removeUrl, '', $path);
        return $removeDomainPath;
    }

    private function getUpdatedDescriptions($oldToNewUrls, $articlesArray) {

        $fields = $this->configureFields();
        foreach ($articlesArray as $key => $articleItem) {

            $longDesc = $articleItem[$fields['description_long']];
            $longDescEng = $articleItem[$fields['description_long_eng']];

            $images = $this->fetchingImages($longDesc);
            
            
            
            /** Updating description with images **/
            $changedLongDesc = $this->updateDesc($longDesc, $images, $oldToNewUrls);
            $changedLongDescEng = $this->updateDesc($longDescEng, $images, $oldToNewUrls);

            /** Removing inline styles from description **/
            $changedLongDesc = $this->updateDescriptionStyles($changedLongDesc);
            $changedLongDescEng = $this->updateDescriptionStyles($changedLongDescEng);
            
            $articlesArray[$key][$fields['description_long']] = $changedLongDesc;
            $articlesArray[$key][$fields['description_long_eng']] = $changedLongDescEng;
        }

        return $articlesArray;
    }

    private function updateDesc($longDesc, $images, $oldToNewUrls) {

        foreach ($images as $image) {
            if (isset($oldToNewUrls[$image])) {
                $longDesc = str_replace($image, $oldToNewUrls[$image], $longDesc);
            }
        }
        return $longDesc;
    }

    private function getOfferPrices() {

        $prices = [];
        $offerPrices = $this->base->readCSV($this->offerCSV);
        foreach ($offerPrices as $offerPrice) {

            $articleNumber = $offerPrice[0];
            $toQuantity = $offerPrice[2];
            $percent = $offerPrice[4];


            if (!isset($prices[$articleNumber])) {
                $prices[$articleNumber][] = ['from' => 1, 'to' => (int) $toQuantity, 'percent' => $percent];
            } else {
                $numberOfPrices = count($prices[$articleNumber]);
                $previousPriceRange = $prices[$articleNumber][$numberOfPrices - 1];
                $previousToQuantiity = $previousPriceRange['to'];
                $currentFromQuantity = $previousToQuantiity + 1;
                $prices[$articleNumber][] = ['from' => $currentFromQuantity, 'to' => (int) $toQuantity, 'percent' => $percent];
            }
        }


        foreach ($prices as $number => $price) {

            $numberOfPrices = count($price);
            $lastPriceRange = $price[$numberOfPrices - 1];
            $lastToQuantiity = $lastPriceRange['to'];
            $currentFromQuantity = $lastToQuantiity + 1;
            $prices[$number][] = ['from' => $currentFromQuantity, 'to' => 'beliebig'];
        }

        return $prices;
    }

    private function getCompletePrices($variantItem) {

        $fields = $this->configureFields();
        $priceAmount = $this->convertPsedoSalesToPrice($variantItem[$fields['pseudo_sales']]);
        $pseudoPriceAmount = $this->convertPsedoSalesToPrice($variantItem[$fields['pseudo_price']]);

        $offerPriceArticles = array_keys($this->offerPrices);

        $orderNumber = $variantItem[$fields['order_number']] != '' ? $variantItem[$fields['order_number']] : $variantItem[$fields['super_product']];

        if (in_array($orderNumber, $offerPriceArticles)) {

            foreach ($this->offerPrices[$orderNumber] as $offerPrice) {

                $priceEK = [
                    'customerGroupKey' => 'EK',
                    'from' => $offerPrice['from'],
                    'to' => $offerPrice['to'],
                    'percent' => isset($offerPrice['percent']) ? $offerPrice['percent'] : 0,
                    'price' => $priceAmount,
                    'pseudoPrice' => $pseudoPriceAmount,
                ];
                $priceH = [
                    'customerGroupKey' => 'H',
                    'from' => $offerPrice['from'],
                    'to' => $offerPrice['to'],
                    'percent' => isset($offerPrice['percent']) ? $offerPrice['percent'] : 0,
                    'price' => $priceAmount,
                    'pseudoPrice' => $pseudoPriceAmount,
                ];

                $prices[] = $priceEK;
                $prices[] = $priceH;
            }
        } else {
            $priceEK = [
                'customerGroupKey' => 'EK',
                'price' => $priceAmount,
                'pseudoPrice' => $pseudoPriceAmount,
            ];
            $priceH = [
                'customerGroupKey' => 'H',
                'price' => $priceAmount,
                'pseudoPrice' => $pseudoPriceAmount,
            ];

            $prices = [$priceEK, $priceH];
        }

        return $prices;
    }

    private function updateDescriptionStyles($str) {

        $changedString = preg_replace('/style=\\"[^\\"]*\\"/', '', $str);
        $removedHeader = str_replace(['<h1>', '</h1>'], '', $changedString);

        return $removedHeader;
    }

}
