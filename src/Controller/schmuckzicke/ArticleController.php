<?php

namespace App\Controller\schmuckzicke;

use App\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package App\Controller
 *
 * @Route("/articles", name="articles_")
 */
class ArticleController extends BaseController {

    protected $apiClient;
    protected $categoryContent;
    protected $articleContent;
    protected $base;

    public function __construct() {
        $this->base = new BaseController();
        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apiKey = '7H18QLRPTeSk78qFyq8HRewENMEk0WB6O7nHWOmi';
        $this->apiClient = $this->base->getClient($url, $username, $apiKey);
    }

    /**
     * @Route("/update-category", name="update_category")
     */
    public function updateCategories() {
        $oldCategory = [];
        $newCategory = [];
        $this->categoryContent = $this->base->readCSV('categories_schmuckzicke.csv');
        foreach ($this->categoryContent as $category) {
            $oldCategory[] = $category[0];
            $newCategory[] = $category[2];
        }

        $this->articleContent = $this->base->readCSV('articles_schmuckzicke.csv');
//        $this->articleContent = $this->base->readCSV('articles_schmuckzicke_sample.csv');
        foreach ($this->articleContent as $index => $item) {
            if (in_array($item[19], $oldCategory) && $item[19] != '') {
                $categoryKey = array_search($item[19], $oldCategory);
                $this->articleContent[$index][19] = $newCategory[$categoryKey];
            }

            if (in_array($item[20], $oldCategory) && $item[20] != '') {
                $categoryKey = array_search($item[20], $oldCategory);
                $this->articleContent[$index][20] = $newCategory[$categoryKey];
            }

            if (in_array($item[21], $oldCategory) && $item[21] != '') {
                $categoryKey = array_search($item[21], $oldCategory);
                $this->articleContent[$index][21] = $newCategory[$categoryKey];
            }

            if (in_array($item[22], $oldCategory) && $item[22] != '') {
                $categoryKey = array_search($item[22], $oldCategory);
                $this->articleContent[$index][22] = $newCategory[$categoryKey];
            }

            if (in_array($item[23], $oldCategory) && $item[23] != '') {
                $categoryKey = array_search($item[23], $oldCategory);
                $this->articleContent[$index][23] = $newCategory[$categoryKey];
            }
        }

        return $this->articleContent;
    }

    /**
     * @Route("/prepare", name="prepare")
     */
    public function prepareArticle() {

        $articleBatch = [];
        $article = [];
        $articleContent = $this->updateCategories();
        foreach ($articleContent as $key => $item) {
            $article['name'] = $item[2];
            $article['descriptionLong'] = $item[3];
            $article['active'] = true;
            $article['supplier'] = $item[10];
            $article['tax'] = $item[11];

            $categories = [];
            if ($item[19] != '') {
                array_push($categories, ['id' => $item[19]]);
            }
            if ($item[20] != '') {
                array_push($categories, ['id' => $item[20]]);
            }
            if ($item[21] != '') {
                array_push($categories, ['id' => $item[21]]);
            }
            if ($item[22] != '') {
                array_push($categories, ['id' => $item[22]]);
            }
            if ($item[23] != '') {
                array_push($categories, ['id' => $item[23]]);
            }

            $article['categories'] = $categories;


            $images = [];
            if ($item[4] != '') {
                array_push($images, ['link' => $item[4]]);
            }
            if ($item[5] != '') {
                array_push($images, ['link' => $item[5]]);
            }
            if ($item[6] != '') {
                array_push($images, ['link' => $item[6]]);
            }
            if ($item[7] != '') {
                array_push($images, ['link' => $item[7]]);
            }
            if ($item[8] != '') {
                array_push($images, ['link' => $item[8]]);
            }
            if ($item[9] != '') {
                array_push($images, ['link' => $item[9]]);
            }


            $inStock = $item[24] === 'enabled' ? 0 : 1;

            $mainDetail = [];
            $mainDetail['number'] = $item[1];
            $mainDetail['inStock'] = $inStock;
            $mainDetail['active'] = true;


            $prices = [];
            $priceEK = [
                'customerGroupKey' => 'EK',
                'price' => $item[12],
                'pseudoPrice' => $item[13],
                'basePrice' => $item[14]
            ];
            $priceH = [
                'customerGroupKey' => 'H',
                'price' => $item[15],
                'pseudoPrice' => $item[16],
                'basePrice' => $item[17]
            ];

            $prices = [$priceEK, $priceH];
            $mainDetail['prices'] = $prices;


            $article['images'] = $images;
            $article['mainDetail'] = $mainDetail;
            $articleBatch[] = $article;
        }
        echo "==========================================================.";
        echo "<br/>";
        echo "Content prepared.";
        echo count($articleBatch) . " items to post";
        return $articleBatch;
    }

    /**
     * @Route("/post", name="post")
     */
    public function postArticle() {

        $articles = $this->prepareArticle();
        $errorArray = [];
        foreach ($articles as $article) {
            $status = $this->apiClient->post('articles', $article);
            if (!$status['success']) {
                echo $article['mainDetail']['number'];
                $errorArray[] = $article['mainDetail']['number'];
            }
        }
        if (count($errorArray) == 0) {
            echo "Every entries updated successfully";
        } else {
            echo "Following entries need to check";
            dump($errorArray);
        }
        die;
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"GET", "DELETE"})
     */
    public function remove($id) {
        $status = $this->apiClient->delete('articles/' . $id);
        dump($status);
        die;
    }

    /**
     * @Route("/delete-all/{from}/{to}", name="delete_all")
     */
    public function removeAll($from, $to) {
        $ids = range($from, $to);
        foreach ($ids as $id) {
            $status = $this->apiClient->delete('articles/' . $id);
        }
        var_dump("Deleted entries.");
        die;
    }

    /**
     * @Route("/property-group", name="property_group")
     */
    public function writePropertyGroup() {
        $properties = array(
            "name" => "Color",
            'position' => 1,
            'comparable' => 1,
            'sortmode' => 2
        );

        $this->apiClient->post('propertyGroups', $properties);
        dump($properties);
        die;
    }

}
