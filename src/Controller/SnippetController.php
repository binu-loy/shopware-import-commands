<?php

namespace App\Controller;

use App\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BaseController
 * @package App\Controller
 *
 * @Route("/snippet", name="snippet_")
 */
class SnippetController extends BaseController
{
    CONST EXISTLOCALE = 'de_DE';
    CONST NEWLOCALE = 'af_NA';

    CONST EXISTDATA = [
        'dirty' => true,
        'namespace' => 'backend/plugin_manager/translation',
        'shopId' => 1,
        'localeId' => SnippetController::EXISTLOCALE,
        'name' => 'expired_plugins_popup/cancel',
        'value' => 'Update english content',
        'created' => '2018-12-03T08:40:48+0100',
        'updated' => '2018-12-03T08:40:48+0100',
    ];
    CONST NEWDATA = [
        'dirty' => true,
        'namespace' => 'backend/plugin_manager/translation',
        'shopId' => 1,
        'localeId' => SnippetController::NEWLOCALE,
        'name' => 'expired_plugins_popup/cancel',
        'value' => 'Create new entry for locale',
        'created' => '2018-12-03T08:40:48+0100',
        'updated' => '2018-12-03T08:40:48+0100',
    ];

    protected $apiClient;

    public function __construct()
    {
        $baseController = new BaseController();
        $this->apiClient = $baseController->getClient();
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {

        $result = $this->apiClient->get('snippets/');
        dump($result);
        die;
    }



    /**
     * @Route("/save", name="save")
     */
    public function save()
    {
        $item = SnippetController::NEWDATA;
        $result = $this->apiClient->post('snippets', $item);
        dump($result);
        die;
    }

    /**
     * @Route("/update", name="update")
     */
    public function update()
    {
        $item = SnippetController::EXISTDATA;
        $result = $this->apiClient->post('snippets', $item);
        dump($result);
        die;
    }

    /**
     * @Route("/delete", name="delete")
     */
    public function delete()
    {
        $result = $this->apiClient->delete('snippets/18603');
        dump($result);
        die;
    }

    /**
     * @Route("/put", name="put")
     */
    public function put()
    {
        $item = SnippetController::EXISTDATA;
        $result = $this->apiClient->put('snippets/18603', $item);
        dump($result);
        die;
    }


}