<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\BaseController;

/**
 * Class BaseController
 * @package App\Controller
 *
 * @Route("/misc", name="misc_")
 */
class MiscController extends AbstractController {

    /**
     * @Route("/find-url", name="find_url")
     */
    public function checkImageUrlInPara() {
        $str = '<table border="0">
	<tbody>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="3">
			<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
				<tbody>
					<tr>
						<td><span style="font-size:14px;"><b>DOSIERGERÄT IR mit Infrarotsensor für das Ausgußbecken</b></span></td>
						<td style="width: 36px;">&nbsp;</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td><img height="1" src="http://www.bdt-dental.de/transparent.gif" width="1" /></td>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td><img height="20" src="http://www.bdt-dental.de/transparent.gif" width="1" /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td style="text-align: justify" valign="top">
			<p>Durch den Einsatz unseres IR-Dosiergerätes ist eine einfache, wirkungsvolle und praxisgerechte Anwendung unseres Dentaflocs gewährleistet.</p>

			<p>&nbsp;</p>
			</td>
			<td>&nbsp;</td>
			<td rowspan="2" valign="top"><a href="http://www.bdt-dental.de/artikelbilder/art_nr_20153_v__i_1.jpg" target="_blank"><img border="0" src="http://www.bdt-dental.de/artikelbilder/art_nr_20153_v__i_1.jpg" width="390" /></a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<table border="0" style="width: 228px;">
				<tbody>
					<tr>
						<td style="background-color: #D8E4F4"><img height="8" src="http://www.bdt-dental.de/transparent.gif" width="20" />Technische Daten</td>
					</tr>
					<tr>
						<td style="background-color: #FFFBCB">
						<table border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td><img height="1" src="http://www.bdt-dental.de/transparent.gif" width="20" /></td>
									<td>
									<table border="0">
										<tbody>
											<tr>
												<td>Breite:</td>
												<td>70 mm</td>
											</tr>
											<tr>
												<td>Höhe:</td>
												<td>120 mm</td>
											</tr>
											<tr>
												<td>Tiefe:</td>
												<td>100 mm</td>
											</tr>
											<tr>
												<td>El. Wert:</td>
												<td>230 V/ 50 Hz</td>
											</tr>
											<tr>
												<td>Gewicht:</td>
												<td>500 g</td>
											</tr>
											<tr>
												<td colspan="2">&nbsp;</td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td>
			<table border="0">
			</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td valign="top"><img border="0" src="http://www.bdt-dental.de/artikelbilder/art_nr_20153_v__i_2.jpg" width="200" /></td>
			<td>&nbsp;</td>
			<td valign="top">&nbsp;</td>
		</tr>
	</tbody>
</table>';

        $base = new BaseController();
        $url = 'http://localhost/teamsportandmore/api';
        $username = 'loyadmin';
        $apikey = 'CIx5orc8HrfWgW339kzI9z71htvoo60vQz05U1Tl';
        $client = $base->getClient($url, $username, $apikey);
        $doc = new \DOMDocument();
        @$doc->loadHTML($str);

        $tags = $doc->getElementsByTagName('img');
        foreach ($tags as $key => $tag) {
//            echo $tag->getAttribute('src');
            $mediaInfo = [
                'album' => -1,
                'file' => $tag->getAttribute('src'),
                'name' => $key,
                'description' => $tag->getAttribute('src')
            ];
            $status = $client->post('media', $mediaInfo);
            if(is_array($status))   {
                $id = $status['data']['id'];
                $insertedImage = $client->get('media/' . $id);
                $path = $insertedImage['data']['path'];
                dump($path);
            }
        }
        die;
    }
    
    
    

}
