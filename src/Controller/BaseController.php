<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\ApiClient;

/**
 * Class BaseController
 * @package App\Controller
 *
 * @Route("/base", name="base_")
 */
class BaseController extends AbstractController {

    protected $apiClient;
    protected $apikeyArray;
    protected $currentHost;
    public $publicPath;
    public $host;

    /**
     * @Route("/client", name="client")
     */
    public function getClient($url = 'http://localhost/teamsportandmore/api', $username = 'loyadmin', $apikey = 'NFhT0ugU8MFDJqVzJH6XvfvnJg2krqaZzJzO5caH', $showSuccessMsg = true) {
        $client = new ApiClient($url, $username, $apikey, $showSuccessMsg);

        $this->apiClient = $client;

        return $client;
    }

    public function readCSV($fileName, $separator = ';') {
        $row = 0;
        $column = 0;
        $content = [];
        if (($handle = fopen($fileName, "r")) !== FALSE) {
//            while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $separator)) !== FALSE) {
                foreach ($data as $index => $column) {
                    $content[$row][$index] = $column;
                }
                $row++;
            }
            fclose($handle);
        }
        return $content;
    }

    public function combineFields($content, array $fields, $useP) {

        $combined = '';
        foreach ($fields as $field) {
            if ($useP) {
                $combined .= '<p>' . $content[$field] . '</p>';
            } else {
                $combined .= $content[$field];
            }
        }
        return $combined;
    }

    public function sonderzeichen($string) {
        $string = str_replace("ä", "a", $string);
        $string = str_replace("ü", "u", $string);
        $string = str_replace("ö", "o", $string);
        $string = str_replace("Ä", "A", $string);
        $string = str_replace("Ü", "U", $string);
        $string = str_replace("Ö", "O", $string);
        $string = str_replace("ß", "ss", $string);
        $string = str_replace("´", "", $string);
        return $string;
    }

    public function writeLog($content, $isString = false, $loc = null) {
        $today = new \DateTime();
        $fileName = $today->format('d-M-Y') . '.txt';
        $publicPath = __DIR__ . "/../../public/logs/";
        $logFileName = $publicPath . $fileName;

        if (!$isString) {
            $content = serialize($content);
        }

        if ($loc == 'T') {
            file_put_contents($logFileName, "================================= \n", FILE_APPEND | LOCK_EX);
        }
        file_put_contents($logFileName, $content . "\n", FILE_APPEND | LOCK_EX);

        if ($loc == 'B') {
            file_put_contents($logFileName, "================================= \n", FILE_APPEND | LOCK_EX);
        }
    }

}
