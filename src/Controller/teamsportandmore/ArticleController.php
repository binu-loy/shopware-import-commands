<?php

namespace App\Controller\teamsportandmore;

use App\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 * @package App\Controller
 *
 * @Route("/team-article", name="team_article_")
 */
class ArticleController extends BaseController {

    protected $apiClient;
    protected $categoryContent;
    protected $articleContent;
    protected $base;

    public function __construct() {
        $host = 'localhost';
        $this->base = new BaseController();
        $this->apiClient = $this->base->getClient($host);
    }

    /**
     * @Route("/image", name="image")
     */
    public function postImages() {
        $this->articleContent = $this->base->readCSV('jako_teamsportandmore.csv');
        foreach ($this->articleContent as $key => $item) {

            $mediaData = [
                'album' => -1,
                'file' => $this->base->publicPath . 'images/teamsportandmore/' . $item[11],
                'name' => $item[1] . '-' . $item[8],
                'description' => $item[1] . '-' . $item[8]
            ];
            $status = $this->apiClient->post('media', $mediaData);
            dump($status);
            dump($item[1] . '-' . $item[8]);
        }
        die;
        dump($this->articleContent);
        die;
    }

    /**
     * Check whether all images in the csv exist in the given set of images
     * @Route("/image-exist", name="image_exist")
     */
    public function checkImageExist() {

        $this->articleContent = $this->base->readCSV('jako_teamsportandmore.csv');
        $missingArray = [];
        $file = fopen("missing.csv", "w");
        foreach ($this->articleContent as $key => $item) {
            if (!file_exists($this->base->publicPath . 'images/teamsportandmore/' . $item[11])) {
                if (!in_array($item[11], $missingArray)) {
                    $missingArray[] = $item[11];
                    fputcsv($file, explode(',', $item[11]));
                }
            }
        }
        fclose($file);
        $file = fopen("missing_articles.csv", "w");
        $articleIds = array_map(function($image) {
            $splittedName = explode('_', $image);
            return $splittedName[0];
        }, $missingArray);
        $articles = array_unique($articleIds);
        foreach ($articles as $article) {
            fputcsv($file, [$article]);
        }
        dump($missingArray);
        dump($articles);
        die;
        fclose($file);
        die;
    }

    /**
     * Check whether all images in the csv exist in the given set of images
     * @Route("/remove-media", name="remove_media")
     */
    public function removeModia() {

        $ids = range(40430, 40445);
        $url = 'https://www.teamsportandmore.de/stage/api';
        $username = 'admin';
        $apiKey = 'bTG2fzEgcv7KZ8r45SKTxWNN06b2fd7R3CHZl5zT';
        $client = $this->base->getClient($url, $username, $apiKey);
        foreach ($ids as $id) {
            $status = $client->delete('media/' . $id);
            dump($status);
        }
        die;
    }

}
